package de.ghoe.guenter.app.mcast.cmd.netusb;

public class Cmd_ClearMcPlaylist extends CmdNetUsb
{
    public Cmd_ClearMcPlaylist( String param)
    {
        m_Cmd = CMD_NAME + "?bank=" + param;
    }

    public Cmd_ClearMcPlaylist( int list)
    {
        m_Cmd = CMD_NAME + "?bank=" + (list+1);
    }

    @Override
    public String getCmd()
    {
        return m_Cmd;
    }

    private static String		m_Cmd;

    public static final String CMD_NAME = "clearMcPlaylist";
    public static final String CMD_DESC = "Clear play-list";
}