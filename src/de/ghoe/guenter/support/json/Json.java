package de.ghoe.guenter.support.json;

import de.ghoe.guenter.support.json.support.StringParser;
import de.ghoe.guenter.support.log.Log;

public abstract class Json
{
	protected Json( String name)
	{
		m_Name = name;
	}
	
	public abstract Object getObject();
	public abstract String getString();

	public String getText( String prefix)
	{
		return prefix + getString() + "\n";
	}

	public String getName()
	{
		return m_Name;
	}
	
	public static JsonHash s_createObject( String input) throws Exception
	{
		StringParser sp = new StringParser(input);
		
		if (!sp.startsWith( '{')) throw new Exception( "JSON file is corrupted: " + sp.getContent());
		return new JsonHash( "", sp);
	}
	
	public static Json s_createObject( String sep, StringParser sp) throws Exception
	{
		Log.s_logDebug( "CreateObject: " + sp.getContent());	
		if (sp.startsWith( "}")) return null; // end of hash -> String will be removed - char not!
		if (sp.startsWith( "]")) return null; // end of array -> String will be removed - char not!
		if (sp.startsWith( sep))
		{
			String name = sp.getName();			
			if (sp.startsWith( '{')) return new JsonHash( name, sp);
			if (sp.startsWith( '[')) return new JsonArray( name, sp);
			if (sp.startsWith( '"')) return new JsonString( name, sp.getString());
			if (sp.startsWith( "true")) return new JsonBool( name, true);
			if (sp.startsWith( "false")) return new JsonBool( name, false);
			String text = sp.getFloat();
			if (null != text) return new JsonFloat( name, Double.parseDouble( text));
			text = sp.getNumber();
			if (null != text) return new JsonNumber( name, Long.parseLong( text));
		}
		throw new Exception( "JSON file is corrupted: " + sp.getContent());
	}
	
	private final String 						m_Name;
	
	protected static final String		PREFIX = "  ";
}
