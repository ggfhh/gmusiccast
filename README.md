**Application to manage playlist on MusicCast devices**
The available music content is shown in a tree, and can be inserted to a 
playlist. This playlist ca be stored on a PC. Main topic is to fix problems when
content of a server has changed and the device playlist get corrupted.

- find: Repeat search for available devices (box on the left select the current device)
- playlist: List of playlists found on device (names can be changed)
- sync: Is used to load playlist from device and correct assignement
- update: Send playlist to device, include update of playlist name 
- load: Load playlist from filesystem
- save: Save playlist to filesystem
- add:  Add selcted folders or tracks to current playlist
- add n: Same as add, but only new entries are added
- remove: Remove entries selected within playlist
- clear: Clear current playlist
- up: Move entries selected in playlist up
- down: Move entries selected in playlist down
- rand: Randomize playlist
