package de.ghoe.guenter.app.mcast.cmd.netusb;

import de.ghoe.guenter.app.mcast.cmd.CmdException;

public class Cmd_ManageList extends CmdNetUsb
{
	public enum Type { AddToPlayList, RemoveFromPlayList}

	public Cmd_ManageList( String param) throws CmdException
	{
		String[] p = param.split( "\\s+");
		if (3 != p.length) throw new CmdException("Invalid count of parameters, need 3 provided " + param);
		m_Cmd = CMD_NAME + "?list_id=main&type=" + p[0] + "&index=" + p[1] + "&timeout=60000&zone=main&bank=" + p[2];
	}

	public Cmd_ManageList( Type t, int trackIndex, int playlistIndex)
	{
		m_Cmd = CMD_NAME + "?list_id=main&type=" + s_getCmdString( t) + "&index=" + trackIndex + "&timeout=60000&zone=main&bank=" + (playlistIndex+1);
	}

	@Override
	public String getCmd()
	{
		return m_Cmd;
	}

	private static String s_getCmdString( Type t)
	{
		switch( t)
		{
			case AddToPlayList:
				return "add_to_mc_playlist";
			case RemoveFromPlayList:
				return "rem_from_mc_playlist";
			default:
				return "<invalid type " + t + ">";
		}
	}

	private final String 	m_Cmd;

	public static final String CMD_NAME = "manageList";
	public static final String CMD_DESC = "Manage lists on device";
}
