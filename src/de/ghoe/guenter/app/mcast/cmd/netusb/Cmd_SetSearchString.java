package de.ghoe.guenter.app.mcast.cmd.netusb;


public class Cmd_SetSearchString extends CmdNetUsb
{
	public Cmd_SetSearchString()
	{
		m_Cmd = CMD_NAME + "?list_id=\"auto_complete\"&string=\"\"";
	}
	
	public Cmd_SetSearchString( String search)
	{
		m_Cmd = CMD_NAME + "?list_id=\"auto_complete\"&string=\"" + search + '"';
	}

	public Cmd_SetSearchString( String search, String op)
	{
		m_Cmd = CMD_NAME + "?list_id=\"" + op + "\"&string=\"" + search + '"';
	}

	@Override
	public String getCmd()
	{
		return m_Cmd;
	}
	
	private final String	m_Cmd;

	public static final String CMD_NAME = "setSearchString";
	public static final String CMD_DESC = "Set Search string";
}
