package de.ghoe.guenter.support.json;

import java.util.HashMap;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.ghoe.guenter.support.log.Log;

public class JsonTemp
{
	public JsonTemp( String in) throws Exception
	{
		if (('{' != in.charAt(0)) && ('}' != in.charAt( in.length()-1))) throw new Exception("Not a JSON input");
		in = in.substring(1).replaceFirst("}$", ",");
		m_List = new HashMap<>();
		
		m_StrPattern = Pattern.compile(PATTERN_STRING);
		m_IntPattern = Pattern.compile(PATTERN_LONG);
		m_ValPattern = Pattern.compile(PATTERN_VALUE);
		parseInput(in);
	}
	
	public JsonObject get( String name)
	{
		return m_List.get( name);
	}
	
	public String getString()
	{
		StringBuilder sb = new StringBuilder();
		TreeSet<String> set = new TreeSet<>( m_List.keySet());
		
		for (String key : set)
		{
			JsonObject json = m_List.get( key);
			sb.append( key);
			sb.append( ":");
			sb.append( json.getStringVal());
			sb.append("\n");
		}
		return sb.toString();
	}
		
	private void parseInput( String in) throws Exception
	{
		while( !in.isEmpty())
		{
			Log.s_logInfo( "parse data: |" + in + "|");
			JsonObject.Type t;
			
			Matcher m = m_StrPattern.matcher(in);
			boolean isString = m.lookingAt();
			if (isString)
			{
				t = JsonObject.Type.String;				
			}
			else
			{
				m = m_IntPattern.matcher( in);
				if (m.lookingAt())
				{
					t = JsonObject.Type.Long;
				}
				else
				{
					m = m_ValPattern.matcher(in);
					if (m.lookingAt())
					{
						t = JsonObject.Type.Boolean;
					}
					else
					{
						throw new Exception( "JSON input is invalid |" + in + "|");
					}
				}
			}
			in = m.replaceFirst("");
			String key = m.group(1);
			String val = m.group(2);
			
			JsonObject obj = new JsonObject( val, t);
			m_List.put( key, obj);
		}
	}
	
	private static final String PATTERN_STRING = "^\"([^\"]*)\":\"([^\"]*)\",";
	private static final String PATTERN_LONG   = "^\"([^\"]*)\":(\\d+),";
	private static final String PATTERN_VALUE  = "^\"([^\"]*)\":([^\\s,]*),";
	
	private final Pattern 						m_StrPattern;
	private final Pattern						m_IntPattern;
	private final Pattern						m_ValPattern;
	private final HashMap<String,JsonObject>	m_List;
}
