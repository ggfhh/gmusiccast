package de.ghoe.guenter.support.json;

import java.util.HashMap;

import de.ghoe.guenter.support.json.support.StringParser;

public class JsonHash extends Json
{
	public JsonHash( String name, StringParser sp) throws Exception
	{
		super(name);
		
		m_Hash = new HashMap<>();
		if (!sp.startsWith( "{}"))
		{
			for (Json obj = s_createObject("{", sp); null != obj; obj = s_createObject( ",", sp))
			{
				m_Hash.put( obj.getName(), obj);
			}
		}
	}
	
	public Json getJason( String name)
	{
		return m_Hash.get( name);
	}
	
	public JsonBool getJasonBool( String name)
	{
		return (JsonBool) m_Hash.get( name);
	}
	
	public JsonNumber getJasonNumber( String name)
	{
		return (JsonNumber) m_Hash.get( name);
	}
	
	public JsonString getJasonString( String name)
	{
		return (JsonString) m_Hash.get( name);
	}
	
	public JsonHash getJasonHash( String name)
	{
		return (JsonHash) m_Hash.get( name);
	}
	
	public JsonArray getJasonArray( String name)
	{
		return (JsonArray) m_Hash.get(name);
	}
	
	@Override
	public Object getObject()
	{
		return m_Hash;
	}

	@Override
	public String getString()
	{
		return getText("");
	}
	
	@Override
	public String getText( String prefix)
	{
		StringBuilder sb = new StringBuilder( prefix);
		sb.append( "Hash [");
		sb.append( m_Hash.size());
		sb.append( "]\n");
		prefix += Json.PREFIX;
		for (String name : m_Hash.keySet())
		{
			Json val = m_Hash.get( name);
			sb.append( val.getText( String.format( "%s%s: ", prefix, name)));
		}
		return sb.toString();
	}
	
	
	private final HashMap<String,Json> m_Hash;
}
