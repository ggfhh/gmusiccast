package de.ghoe.guenter.app.gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.List;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;

import de.ghoe.guenter.app.gui.components.GComboBox;
import de.ghoe.guenter.app.gui.components.GContentTree;
import de.ghoe.guenter.app.gui.components.PlayList;
import de.ghoe.guenter.app.gui.components.PlayListEntry;
import de.ghoe.guenter.app.gui.components.support.ComponentSizePreserver;
import de.ghoe.guenter.app.gui.components.support.ResizeComponent;
import de.ghoe.guenter.app.mcast.jobs.JobLoadPlaylist;
import de.ghoe.guenter.app.mcast.jobs.JobSyncFromDevice;
import de.ghoe.guenter.app.mcast.jobs.JobUpdatePlaylist;
import de.ghoe.guenter.support.config.UserConfig;
import de.ghoe.guenter.support.json.Json;
import de.ghoe.guenter.support.log.Log;
import de.ghoe.guenter.app.mcast.data.ContentNode;

public class GuiTab_Browse extends GuiTab
{
	public GuiTab_Browse( JFrame parent, IntfApplication intf)
	{
		super( parent, "Browse", intf);
		m_App = intf;
		m_Frame = parent;

		m_SaveChooser = null;
		m_LoadChooser = null;

		JSplitPane splitpane = new JSplitPane( JSplitPane.HORIZONTAL_SPLIT);
//		splitpane.setPreferredSize( new Dimension( 1000, 600));
		add( splitpane);
		
		// tree folder
		JScrollPane scrollPane = new JScrollPane( );
		new ComponentSizePreserver( scrollPane, UserConfig.TREEVIEW_SIZE, new Dimension( 400, 600));
		m_Content = new GContentTree(scrollPane, intf.getContent(), new MouseAdapter()
		{
			@Override
			public void mouseClicked(MouseEvent e)
			{
				clickedOnTree( e);
			}
		});
		splitpane.add( scrollPane);

		// Play-List
		JPanel p = new JPanel();
//		p.setPreferredSize( new Dimension( 500, 600));
		p.setLayout( new BorderLayout());
		
		JPanel p1 = new JPanel();
		p1.setLayout( new FlowLayout());
		m_SelectPlaylist = new GComboBox( m_App.getPlayListNames());
		ResizeComponent.s_setWidth( m_SelectPlaylist, 240);
		m_SelectPlaylist.setEditable(true);
		p1.add( m_SelectPlaylist);
		JButton b = new JButton( "Sync");
		ResizeComponent.s_setWidth( b, 80);
		b.addActionListener( (ActionEvent e) -> syncFromDevice());
		p1.add( b);
		b = new JButton( "Update");
		b.addActionListener( (ActionEvent e) -> updatePlayList());
		p1.add( b);
		b = new JButton( "Load");
		b.addActionListener( (ActionEvent e) -> loadPlaylist());
		p1.add( b);		
		b = new JButton( "Save");
		b.addActionListener( (ActionEvent e) -> savePlaylist( ));
		p1.add( b);		
		p.add( p1, BorderLayout.NORTH);

		m_PlaylistEntries = PlayList.s_createPlayList(  m_App);
		JScrollPane sp = new JScrollPane( m_PlaylistEntries);
		new ComponentSizePreserver( sp, UserConfig.PLAYLIST_SIZE, new Dimension( 400, 560));
		p.add( sp, BorderLayout.CENTER);
		
		p1 = new JPanel();
		p1.setLayout( new FlowLayout());
		b = new JButton( "Add");
		ResizeComponent.s_setWidth( b, 80);
		b.addActionListener( (ActionEvent e) -> addTracks( true));
		p1.add( b);
		b = new JButton( "Add New");
		ResizeComponent.s_setWidth( b, 80);
		b.addActionListener( (ActionEvent e) -> addTracks( false));
		p1.add( b);
		b = new JButton( "Remove");
		ResizeComponent.s_setWidth( b, 80);
		b.addActionListener( (ActionEvent e) -> removeTracks());
		p1.add( b);
		b = new JButton( "Clear");
		ResizeComponent.s_setWidth( b, 80);
		b.addActionListener( (ActionEvent e) -> clearTracks());
		p1.add( b);
		b = new JButton( "Up");
		ResizeComponent.s_setWidth( b, 80);
		b.addActionListener( (ActionEvent e) -> moveTracksUp());
		p1.add( b);		
		b = new JButton( "Down");
		ResizeComponent.s_setWidth( b, 80);
		b.addActionListener( (ActionEvent e) -> moveTracksDown());
		p1.add( b);
		b = new JButton( "Rand");
		ResizeComponent.s_setWidth( b, 80);
		b.addActionListener( (ActionEvent e) -> moveTracksRandom());
		p1.add( b);
		p.add(p1, BorderLayout.SOUTH);
		
		splitpane.add( p);
	}

	@Override
	public void setResult(String command, Json reply)
	{
		m_SelectPlaylist.setSelectedItem( "Should place correct file-path to this location");
	}
	
	@Override
	public void selectCurrentTab() // default do nothing :-)
	{
		m_Content.setContent( m_App.getContent());
		m_SelectPlaylist.setContent( m_App.getPlayListNames());
	}

	private void updatePlayList()
	{
		Log.s_logInfo("Update playlist on target");
		int playListIdx = m_SelectPlaylist.getLastSelectedIndex();
		if (0 <= playListIdx)
		{
			if (m_App.clearPlayList(playListIdx))
			{
				if (m_App.renamePlayList(playListIdx, m_SelectPlaylist.getSelectedItemString()))
				{
					ProgressInfo progressInfo = new ProgressInfo( m_Frame.getOwner(), super.getWidth());
					JobUpdatePlaylist job = new JobUpdatePlaylist(progressInfo, "Update Playlist", m_PlaylistEntries.getEntries(), playListIdx);
					String failed = job.getFailedList();
					if (null != failed)
					{
						JOptionPane.showMessageDialog(this, failed, "List of Titles not inserted to playlist", JOptionPane.WARNING_MESSAGE);
					}
				}
				else
				{
					JOptionPane.showMessageDialog(this, "Failed to set name of Playlist", "Update Playlist Problem", JOptionPane.ERROR_MESSAGE);
				}
			}
			else
			{
				JOptionPane.showMessageDialog(this, "Failed to clear Playlist", "Update Playlist Problem", JOptionPane.ERROR_MESSAGE);
			}
		}
		else
		{
			JOptionPane.showMessageDialog(this, "No Playlist entry selected", "Update Playlist Problem", JOptionPane.ERROR_MESSAGE);
		}
	}

	private void syncFromDevice()
	{
		ContentNode startNode = getSingleSelectedNode();

		if (null != startNode)
		{
			try
			{
				String[] tracks = m_App.getPlayListContent(m_SelectPlaylist.getSelectedIndex(), m_SelectPlaylist.getSelectedItemString());
				if (null != tracks)
				{
					m_PlaylistEntries.clear();
					ProgressInfo progressInfo = new ProgressInfo( m_Frame.getOwner(), super.getWidth());
					JobSyncFromDevice job = new JobSyncFromDevice(progressInfo, "Sync Playlist", tracks, m_PlaylistEntries, startNode);
					String failedList = job.getFailedList();
					if (null != failedList)
					{
						JOptionPane.showMessageDialog(this, failedList, "List of not found Titles", JOptionPane.WARNING_MESSAGE);
					}
				}
			}
			catch (Throwable e)
			{
				Log.s_log("Failed to sync playlist: ", e);
			}
		}
		else
		{
			JOptionPane.showMessageDialog(this, "Need single selected starting-point within tree", "Failed to sync Playlist", JOptionPane.ERROR_MESSAGE);
		}
	}

	private ContentNode getSingleSelectedNode()
	{
		List<ContentNode> l = m_Content.getSelectedEntries();
		if (1 == l.size())
		{
			return l.get(0);
		}
		return null;
	}

	private void loadPlaylist( )
	{
		if (null == m_LoadChooser)
		{
			m_LoadChooser = new JFileChooser();
			m_LoadChooser.setDialogType( JFileChooser.OPEN_DIALOG);
			m_LoadChooser.setDialogTitle( "Load Playlist");
			m_LoadChooser.addChoosableFileFilter(new FileNameExtensionFilter("Playlist", FILE_EXT));
			m_LoadChooser.setPreferredSize(UserConfig.s_getFileChooserSize( m_LoadChooser.getPreferredSize()));
			m_LoadChooser.setCurrentDirectory(UserConfig.s_getPlaylistFolder( m_LoadChooser.getCurrentDirectory()));
		}
		if (JFileChooser.APPROVE_OPTION != m_LoadChooser.showOpenDialog( this))	return;
		File f = m_LoadChooser.getSelectedFile();
		if (f.canRead())
		{
			ProgressInfo progressInfo = new ProgressInfo( m_Frame.getOwner(), super.getWidth());
			new JobLoadPlaylist( progressInfo, "Load Playlist", m_PlaylistEntries, f);
			UserConfig.s_setFileChooserSize( m_LoadChooser.getSize());
			UserConfig.s_setPlaylistFolder( m_LoadChooser.getCurrentDirectory());
		}
		else
		{
			JOptionPane.showMessageDialog(this, "Failed to load Playlist from file");
		}
	}
	
	private void savePlaylist( )
	{
		if (null == m_SaveChooser)
		{
			m_SaveChooser = new JFileChooser();
			m_SaveChooser.setDialogType( JFileChooser.SAVE_DIALOG);
			m_SaveChooser.setDialogTitle( "Save Playlist");
			m_SaveChooser.addChoosableFileFilter(new FileNameExtensionFilter("Playlist", FILE_EXT));
			m_SaveChooser.setPreferredSize(UserConfig.s_getFileChooserSize( m_SaveChooser.getPreferredSize()));
			m_SaveChooser.setCurrentDirectory(UserConfig.s_getPlaylistFolder( m_SaveChooser.getCurrentDirectory()));
		}
		if (JFileChooser.APPROVE_OPTION != m_SaveChooser.showSaveDialog(this))	return;
		File f = m_SaveChooser.getSelectedFile();
		if (!f.exists() || f.canWrite())
		{
			try
			{
				m_PlaylistEntries.save( f);
				UserConfig.s_setFileChooserSize( m_SaveChooser.getSize());
				UserConfig.s_setPlaylistFolder( m_SaveChooser.getCurrentDirectory());
				return;
			}
			catch ( Throwable e)
			{
				Log.s_log("Failed to save playlist", e);
			}
		}
		JOptionPane.showMessageDialog(this, "Failed to save Playlist to file");
	}

	private void clickedOnTree(MouseEvent e)
	{
		if (2 <= e.getClickCount())
		{
			addTracks( false);
		}
	}

	private void addTracks( boolean all)
	{
		List<ContentNode> l = m_Content.getSelectedEntries();
		Log.s_logInfo("Add " + l.size() + " titles to the playlist");
		for ( ContentNode cn : l)
		{
			Log.s_logVerbose("Add to playlist: " + cn.getExtendedName());
			addAllEntries( cn, all);
		}
	}
	
	private void addAllEntries( ContentNode cn, boolean all)
	{
		if (cn.isLeaf())
		{
			if (all || (!m_PlaylistEntries.hasEntry( cn.getName())))
			{
				m_PlaylistEntries.addEntry( new PlayListEntry( cn));
			}
		}
		else
		{
			int cnt = cn.getChildCount();
			for (int i = 0; i < cnt; ++i)
			{
				addAllEntries( (ContentNode) cn.getChildAt(i), all);
			}
		}
	}

	private void removeTracks()
	{
		checkResult( m_PlaylistEntries.removeSelected());
	}

	private void clearTracks()
	{
		checkResult( m_PlaylistEntries.clear());
	}

	private void moveTracksUp()
	{
		checkResult( m_PlaylistEntries.moveSelectedUp());
	}

	private void moveTracksDown()
	{
		checkResult( m_PlaylistEntries.moveSelectedDown());
	}

	private void moveTracksRandom()
	{
		checkResult( m_PlaylistEntries.randomize());
	}

	private void checkResult( boolean ok)
	{
		if (!ok)
		{
			Toolkit.getDefaultToolkit().beep();
		}
	}
	
	private JFileChooser				m_SaveChooser;
	private JFileChooser				m_LoadChooser;
	
	private final GContentTree			m_Content;
	private final IntfApplication 		m_App;
	
	private final GComboBox				m_SelectPlaylist;
	private final PlayList				m_PlaylistEntries;

	private final JFrame 				m_Frame;

	private static final String			FILE_EXT = "plist";

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
}
