package de.ghoe.guenter.app.gui;

import java.awt.BorderLayout;
import java.awt.Font;

import javax.swing.*;
import javax.swing.border.BevelBorder;

import de.ghoe.guenter.app.gui.components.CommandInputGui;
import de.ghoe.guenter.support.json.Json;

public class GuiTab_Manual extends GuiTab
{
	public GuiTab_Manual(JFrame parent, IntfApplication intf)
	{
		super( parent,"Manual", intf);
		
		add( new CommandInputGui( intf), BorderLayout.NORTH);
		m_Reply = new JTextArea(40,144);
		m_Reply.setFont( new Font( Font.MONOSPACED, Font.PLAIN, 12));		
		m_Reply.setEditable(false);
		m_Reply.setBorder( BorderFactory.createBevelBorder( BevelBorder.LOWERED));
		
		JScrollPane sp = new JScrollPane( m_Reply);
		add( sp, BorderLayout.CENTER);
	}
	
	@Override
	public void setResult(String command, Json reply)
	{
		m_Reply.setText( reply.getText(""));
	}
	
	private final JTextArea		m_Reply;

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
