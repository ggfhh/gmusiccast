package de.ghoe.guenter.app.mcast.cmd;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Vector;

import de.ghoe.guenter.app.mcast.cmd.netusb.CmdNetUsb;
import de.ghoe.guenter.app.mcast.cmd.sys.CmdSys;
import de.ghoe.guenter.support.json.JsonHash;
import de.ghoe.guenter.support.log.Log;
import de.ghoe.guenter.support.net.NetHttpJson;

public abstract class Cmd
{
	protected Cmd()
	{
		m_Json = null;
	}
	
	public abstract String getCmdGroup();
	public abstract String getCmd();
	
	public static String s_getErrorString( int code)
	{
		switch(code)
		{
		case -1: return "-1) Failed to read response";
		case 0: return null;
		case 1: return "1) Initializing";
		case 2: return "2) Internal Error";
		case 3: return "3) Invalid Request (A method did not exist, a method wasn't appropriate etc.)";
		case 4: return "4) Invalid Parameter (out of range, invalid characters etc.)";
		case 5: return "5) Guarded (Unable to setup in current status etc.)";
		case 6: return "6) Time Out";
		case 99: return "99) Firmware Updating";
		case 100: return "100) Streaming Service Access Error";
		case 101: return "101) Streaming Service Other Error";
		case 102: return "102) Streaming Service Wrong User Name";
		case 103: return "103) Streaming Service Wrong Password";
		case 104: return "104) Streaming Service Account Expired";
		case 105: return "105) Streaming Service Account Disconnected/Gone Off/Shut Down";
		case 106: return "106) Streaming Service Account Number Reached to the Limit";
		case 107: return "107) Streaming Service Server Maintenance";
		case 108: return "108) Streaming Service Invalid Account";
		case 109: return "109) Streaming Service License Error";
		case 110: return "110) Streaming Service Read Only Mode";
		case 111: return "111) Streaming Service Max Stations";
		case 112: return "112) Streaming Service Access Denied";
		default:  return "" + code + ") Unknown response"; 
		}		
	}

	public static String s_getErrorStringSave( int code)
	{
		String res = s_getErrorString(code);
		return (null != res) ? res : "0) Success";
	}

	public String getExtension()
	{
		return null;
	}

	public int getCommandResponse()
	{
		try
		{
			int response = m_Json.getJasonNumber("response_code").getIntegerValue();
			Log.s_logInfo("\tResult=" + s_getErrorStringSave(response));
			Log.s_logVerbose("\tContent: " + m_Json.getString());
			return response;
		} 
		catch (Exception e)
		{
			Log.s_log("Failed to get response", e);
		}
		return -1;
	}
	
	public boolean checkResponse()
	{
		return 0 == getCommandResponse();
	}
	
	public String getErrorString()
	{
		return s_getErrorString( getCommandResponse());
	}
	
	public String getResultString() throws CmdException
	{
		return getResult().getText("");
	}

	public void execute( NetHttpJson net) throws Exception
	{
		String request = getCommandString();
		String postData = getExtension();
		if (null == postData)
		{
			Log.s_logInfo("Execute GET-command: " + request);
			m_Json = net.getJson(request);
		}
		else
		{
			Log.s_logInfo("Execute POST-command: " + request + " (" + postData + ")");
			m_Json = net.postJson(request, postData);
		}
	}
	
	public String getCommandString( )
	{
		return CMD_PREFIX + getCmdGroup() + '/' + getCmd();
	}
	
	public static Vector<String> s_getCommandNames()
	{
		s_collectCommands();
		return new Vector<>( s_Commands.keySet());
	}
	
	public static String s_getCommandDescription()
	{
		s_collectCommands();
		return s_CommandsDescription;
	}
	
	public JsonHash getResult() throws CmdException
	{
		if (null == m_Json) throw new CmdException( "Command " + getCommandString() + " not executed!");
		return m_Json;
	}
	
	public static Cmd s_createCommand( String name, String param) throws CmdException, NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException
	{
		Class<?> c = s_Commands.get( name);
		if (null == c) throw new CmdException( "Unknown command \"" + name + "\" requested");
		Constructor<?> ctor = c.getConstructor( String.class);
		
		return (Cmd) (ctor.newInstance( param));
	}
	
	protected static void s_addCommand( StringBuilder sb, Class<?> cmd)
	{
		try
		{
			Field f = cmd.getField("CMD_NAME");
			Object obj = f.get( null);
			String name = (String) obj;
			s_Commands.put(name, cmd);
			f = cmd.getField( "CMD_DESC");
			obj = f.get(null);
			String desc = (String) obj;
			sb.append( "\n\t");
			sb.append( name);
			sb.append( ": ");
			sb.append( desc);
		}
		catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e)
		{
			Log.s_log( "Failed to add Commands", e);
		}
	}

	private static void s_collectCommands()
	{
		if (null == s_Commands)
		{
			StringBuilder sb = new StringBuilder( "CommandDescription:");
			s_Commands = new HashMap<>();
			// add all commands
			CmdSys.s_addCommands( sb);
			CmdNetUsb.s_addCommands( sb);
			
			s_CommandsDescription = sb.toString();
		}
	}
	
	private JsonHash							m_Json;
	
	private static HashMap<String,Class<?> >	s_Commands;	
	private static String						s_CommandsDescription;
	
	private static final String CMD_PREFIX 	= "/YamahaExtendedControl/v1/";
}
