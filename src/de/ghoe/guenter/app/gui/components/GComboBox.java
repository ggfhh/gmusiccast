package de.ghoe.guenter.app.gui.components;

import javax.swing.*;
import java.awt.event.ActionEvent;

public class GComboBox extends JComboBox<String>
{
	public GComboBox( String[] content)
	{
		super( new DefaultComboBoxModel<>( (null != content) ? content : new String[0]));
		m_Content = content;
		m_LastSelected = -1;

		addActionListener(this::doAction);
	}

	public String getSelectedItemString( )
	{
		return (String) this.getSelectedItem();
	}

	public int getLastSelectedIndex() { return m_LastSelected;}

	public void setContent( String[] content)
	{
		if (m_Content != content)
		{
			DefaultComboBoxModel<String> model = (DefaultComboBoxModel<String>) this.getModel();
			model.removeAllElements();
			if (null != content)
			{
				for (String s : content)
				{
					model.addElement(s);
				}
			}
			m_Content = content;
		}
	}

	public void setContent( String value, int idx)
	{
		DefaultComboBoxModel<String> model = (DefaultComboBoxModel<String>) this.getModel();
		int cnt = model.getSize();
		if (cnt > idx)
		{
			model.removeElementAt(idx);
			model.insertElementAt( value, idx);
		}
		else
		{
			model.addElement(value);
		}
	}

	private void doAction( ActionEvent e)
	{
		int idx = getSelectedIndex();
		if (0 <= idx)
		{
			m_LastSelected = idx;
		}
	}

	private String[]	m_Content;
	private int			m_LastSelected;

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
}
