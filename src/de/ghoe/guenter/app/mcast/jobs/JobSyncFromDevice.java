package de.ghoe.guenter.app.mcast.jobs;

import de.ghoe.guenter.app.gui.components.PlayList;
import de.ghoe.guenter.app.gui.components.PlayListEntry;
import de.ghoe.guenter.app.mcast.data.ContentNode;
import de.ghoe.guenter.support.worker.IntfJobProgress;
import de.ghoe.guenter.support.worker.Job;

public class JobSyncFromDevice extends Job
{
	public JobSyncFromDevice(IntfJobProgress progress, String title, String[] tracks, PlayList playlist, ContentNode startNode)
	{
		super(progress, title);
		m_Tracks = tracks;
		m_Playlist = playlist;
		m_StartNode = startNode;
		m_FailedList = null;
		start();
	}

	public String getFailedList()
	{
		return (null != m_FailedList) ? m_FailedList.toString() : null;
	}

	@Override
	public void doWork(IntfJobProgress progress)
	{
		progress.progressSetStepCount( m_Tracks.length);
		for (String track : m_Tracks)
		{
			progress.progressNextStep("Sync: " + track);
			ContentNode node = m_StartNode.findContent(track);
			if (null != node)
			{
				m_Playlist.addEntry(new PlayListEntry(node));
			}
			else
			{
				if (null == m_FailedList)
				{
					m_FailedList = new StringBuilder();
				}
				m_FailedList.append(track);
				m_FailedList.append('\n');
			}
		}
	}

	private final String[]		m_Tracks;
	final PlayList				m_Playlist;
	final ContentNode 			m_StartNode;
	StringBuilder			m_FailedList;
}
