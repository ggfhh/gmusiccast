package de.ghoe.guenter.app.gui.components.support;

import java.util.ArrayList;
import java.util.List;

import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;

public class GTreeModel implements TreeModel
{
	public GTreeModel( TreeNode root)
	{
		m_Root = root;
		m_Listener = new ArrayList<>();
	}

	@Override
	public TreeNode getRoot()
	{
		return m_Root;
	}

	@Override
	public TreeNode getChild(Object parent, int index)
	{
		TreeNode obj = (TreeNode) parent;
		return obj.getChildAt(index);
	}

	@Override
	public int getChildCount(Object parent)
	{
		TreeNode obj = (TreeNode) parent;
		return obj.getChildCount();
	}

	@Override
	public boolean isLeaf(Object node)
	{
		TreeNode obj = (TreeNode) node;
		return obj.isLeaf();
	}

	@Override
	public void valueForPathChanged(TreePath path, Object newValue)
	{
		System.out.println( "Value of path '" + path.toString() + "' set to new value '" + newValue.toString() + "'");
		for (TreeModelListener e : m_Listener)
		{
			System.out.println( "\tListener: " + e.toString());
		}
	}

	@Override
	public int getIndexOfChild(Object parent, Object child)
	{
		TreeNode p = (TreeNode) parent;
		TreeNode c = (TreeNode) child;
		
		int idx = p.getChildCount();
		while( 0 < idx--)
		{
			TreeNode t = p.getChildAt(idx);
			if (t == c) break;
		}
		return idx;
	}

	@Override
	public void addTreeModelListener(TreeModelListener l)
	{
		if (!m_Listener.contains(l))
		{
			m_Listener.add( l);
		}
	}

	@Override
	public void removeTreeModelListener(TreeModelListener l)
	{
		m_Listener.remove( l);
	}
	
	private final TreeNode					m_Root;
	private final List<TreeModelListener>	m_Listener;
}
