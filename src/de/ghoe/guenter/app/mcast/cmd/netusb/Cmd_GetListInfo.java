package de.ghoe.guenter.app.mcast.cmd.netusb;

import de.ghoe.guenter.app.mcast.cmd.CmdException;

public class Cmd_GetListInfo extends CmdNetUsb
{	
	public Cmd_GetListInfo( String input) throws CmdException
	{
		String[] entries = input.split("\\s+");
		
		switch( entries.length)
		{
		case 1:
			m_Cmd = CMD_NAME + "?input=" +  entries[0] + "&index=0&size=8";
			break;
		case 2:
			m_Cmd = CMD_NAME + "?input=" + entries[0] + "&index=" + entries[1] + "&size=8";
			break;
		case 3:
			m_Cmd = CMD_NAME + "?input=" + entries[0] + "&index=" + entries[1] + "&size=" + entries[2];
			break;
		default:
			throw new CmdException( "Invalid parameter for command " + CMD_NAME + ": <input> [<offset> [<count>]]");
		}
	}

	public Cmd_GetListInfo( String input, int off)
	{
		m_Cmd = CMD_NAME + "?input=" + input + "&index=" + off + "&size=8";
	}

	public Cmd_GetListInfo( String input, int off, int size)
	{
		m_Cmd = CMD_NAME + "?input=" + input + "&index=" + off + "&size=" + size;
	}

	@Override
	public String getCmd()
	{
		return m_Cmd;
	}

	private final String		m_Cmd;
	
	public static final String CMD_NAME = "getListInfo";
	public static final String CMD_DESC = "Read List information";
}
