package de.ghoe.guenter.support.json.support;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringParser
{
	public StringParser( String in)
	{
		// need to translate input -> to
		m_String = s_fixContent( in);
	}
	
	public String getText()
	{
		Pattern p = Pattern.compile( "^([^,}]*)");
		Matcher m = p.matcher( m_String);
		if (m.matches())
		{
			m_String = m.replaceFirst( "");
			return s_restoreContent( m.group(1));
		}
		return null;
	}
	
	public boolean startsWith( char z)
	{
		return (!m_String.isEmpty() && (z == m_String.charAt(0)));
	}
	
	public boolean startsWith( String s)
	{
		if (m_String.startsWith( s))
		{
			m_String = m_String.substring( s.length());
			return true;
		}
		return false;
	}
		
	public String getName()
	{
		return s_restoreContent( replaceFirst( PATTERN_NAME));
	}
	
	public String getString()
	{
		return replaceFirst( PATTERN_STRING);
	}

	public String getFloat()
	{
		String res = replaceFirst(PATTERN_FLOAT);
		if (null == res)
		{
			res = replaceFirst(PATTERN_FLOATE);
		}
		return s_restoreContent( res);
	}

	public String getNumber( )
	{
		return s_restoreContent( replaceFirst( PATTERN_NUMBER));
	}
	
	public String getContent()
	{
		return s_restoreContent( m_String);
	}
	
	private String replaceFirst( Pattern p)
	{
		Matcher m = p.matcher( m_String);
		if (!m.lookingAt()) return null;
		m_String = m.replaceFirst( "");
		return s_restoreContent( m.group(1));
	}

	private static String s_fixContent( String in)
	{
		if (null != in)
		{
			return in.replaceAll("\\\\\"", FIX_PREFIX_STRING);
		}
		return null;
	}

	private static String s_restoreContent( String in)
	{
		if (null != in)
		{
			return in.replaceAll( FIX_PREFIX_STRING, "\\\"");
		}
		return null;
	}
		
	private String  m_String;

	private static final String FIX_PREFIX_STRING	= "__GH_do_replace_escape_String_";

	private static final String	PARSE_NAME   = "^\"([^\"]*)\":";
	private static final String PARSE_STRING = "^\"([^\"]*)\"";
	private static final String PARSE_FLOAT  = "^-?(\\d+\\.\\d*)";
	private static final String PARSE_FLOATE = "^-?(\\d+\\.\\d+[eE]-?\\d+)";
	private static final String	PARSE_NUMBER = "^-?(\\d+)";
	
	private static final Pattern PATTERN_NAME   = Pattern.compile(PARSE_NAME);
	private static final Pattern PATTERN_STRING = Pattern.compile(PARSE_STRING);
	private static final Pattern PATTERN_FLOAT  = Pattern.compile(PARSE_FLOAT);
	private static final Pattern PATTERN_FLOATE = Pattern.compile(PARSE_FLOATE);
	private static final Pattern PATTERN_NUMBER = Pattern.compile(PARSE_NUMBER);
}
