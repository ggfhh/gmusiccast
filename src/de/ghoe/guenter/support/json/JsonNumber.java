package de.ghoe.guenter.support.json;

public class JsonNumber extends Json
{
	public JsonNumber( String name, long val)
	{
		super(name);
		
		m_Val = val;
	}
	
	@Override
	public Object getObject()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getString( )
	{
		return m_Val.toString();
	}

	public long getLongValue()
	{
		return m_Val;
	}
	
	public int getIntegerValue() throws Exception
	{
		if (Integer.MAX_VALUE < m_Val) throw new Exception("Response value (" + m_Val + ") is to large for an integer.");
		if (Integer.MIN_VALUE > m_Val) throw new Exception("Response value (" + m_Val + ") is to small for an integer.");
		return m_Val.intValue();
	}

	public short getShortValue() throws Exception
	{
		if (Short.MAX_VALUE < m_Val) throw new Exception("Response value (" + m_Val + ") is to large for an short integer.");
		if (Short.MIN_VALUE > m_Val) throw new Exception("Response value (" + m_Val + ") is to small for an short integer.");
		return m_Val.shortValue();
	}

	public short getByteValue() throws Exception
	{
		if (Byte.MAX_VALUE < m_Val) throw new Exception("Response value (" + m_Val + ") is to large for an byte.");
		if (Byte.MIN_VALUE > m_Val) throw new Exception("Response value (" + m_Val + ") is to small for an byte.");
		return m_Val.shortValue();
	}

	private final Long m_Val;
}
