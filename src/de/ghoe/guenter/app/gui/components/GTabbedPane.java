package de.ghoe.guenter.app.gui.components;

import javax.swing.JTabbedPane;

import de.ghoe.guenter.app.gui.GuiTab;
import de.ghoe.guenter.support.log.Log;

public class GTabbedPane extends JTabbedPane
{
	public GTabbedPane()
	{
		addChangeListener( (ChangeEvent) -> changeCurrentTab());
		m_CurrTab = null;
	}
	
	public GuiTab getCurrentTab()
	{
		if (null == m_CurrTab)
		{
			m_CurrTab = (GuiTab) super.getSelectedComponent();
		}
		return m_CurrTab;
	}

	private void changeCurrentTab()
	{
		GuiTab newCurr = (GuiTab) super.getSelectedComponent();
		if (null != newCurr)
		{
			Log.s_logVerbose( "Select Tab: " + newCurr.getTitle());
			if (newCurr != m_CurrTab)
			{
				m_CurrTab = newCurr;
				m_CurrTab.selectCurrentTab();
			}
		}
	}
	
	private GuiTab			m_CurrTab;

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


}
