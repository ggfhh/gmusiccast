package de.ghoe.guenter.app.gui;

import java.awt.BorderLayout;

import javax.swing.*;

import de.ghoe.guenter.support.json.Json;

public abstract class GuiTab extends JPanel
{
	public GuiTab(JFrame parent, String title, IntfApplication intf)
	{
		this.setLayout( new BorderLayout());
		m_Title = title;
		m_Appl = intf;
	}
	
	public abstract void setResult( String command, Json reply);
	
	public void selectCurrentTab() // default do nothing :-)
	{}

	public String getTitle()
	{
		return m_Title;
	}
	
	protected IntfApplication getApplication()
	{
		return m_Appl;
	}

	private final String			m_Title;
	private final IntfApplication	m_Appl;

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
}
