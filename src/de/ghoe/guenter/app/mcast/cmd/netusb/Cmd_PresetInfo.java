package de.ghoe.guenter.app.mcast.cmd.netusb;

public class Cmd_PresetInfo extends CmdNetUsb
{

	public Cmd_PresetInfo( String param)
	{
	}

	@Override
	public String getCmd()
	{
		return CMD_NAME;
	}

	public static final String CMD_NAME = "getPresetInfo";
	public static final String CMD_DESC = "Read Preset information";
}
