package de.ghoe.guenter.app.gui.components.support;

import de.ghoe.guenter.support.config.UserConfig;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;

public class ComponentSizePreserver implements ComponentListener
{
	public ComponentSizePreserver(JComponent comp, String key, Dimension prefSize)
	{
		m_Key = key;
		m_Comp = comp;
		m_Comp.setPreferredSize(UserConfig.s_get( m_Key, prefSize));
		m_Comp.addComponentListener( this);
	}

	@Override
	public void componentResized(ComponentEvent e)
	{
		UserConfig.s_set( m_Key, m_Comp.getSize());
	}

	@Override
	public void componentMoved(ComponentEvent e)
	{ }

	@Override
	public void componentShown(ComponentEvent e)
	{ }

	@Override
	public void componentHidden(ComponentEvent e)
	{ }

	private final JComponent 	m_Comp;
	private final String 		m_Key;
}
