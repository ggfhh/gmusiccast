package de.ghoe.guenter.app.mcast.cmd.sys;

public class Cmd_GetDeviceInfo extends CmdSys
{
	public Cmd_GetDeviceInfo(String param)
	{
	}

	@Override
	public String getCmd()
	{
		return CMD_NAME;
	}

	public static final String CMD_NAME = "getDeviceInfo";
	public static final String CMD_DESC = "Get information about device";
}
