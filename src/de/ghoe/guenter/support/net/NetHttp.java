package de.ghoe.guenter.support.net;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.ghoe.guenter.support.log.Log;
import de.ghoe.guenter.support.net.support.HostInfo;

public class NetHttp
{
	//*****************************************************
	// Constructors
	//*****************************************************
	public NetHttp( InetAddress addr)
	{
		m_Address = addr;
		m_Host = addr.getHostAddress();
		m_Port = DEFAULT_PORT;
	}

	public NetHttp( InetAddress addr, int port)
	{
		m_Address = addr;
		m_Host = addr.getHostAddress();
		m_Port = port;
	}
	
	//*****************************************************
	// Public methods
	//*****************************************************
	public String sendGet( String request) throws IOException, HttpException
	{
		String data = "GET " + request + USED_PROTOCOL + CONTENT_TEXT + "Host: " + m_Host + NEWLINE + AGENT_CONFIG + NEWLINE;
		return sendAndRxData( data);
	}

	public String sendPost( String request, String param) throws IOException, HttpException
	{
		String data = "POST " + request + USED_PROTOCOL + CONTENT_TEXT + OPT_CLOSECON + "Host: " + m_Host + NEWLINE
					+ AGENT_CONFIG + "Content-Length: " + param.length() + NEWLINE + NEWLINE + param;
		return sendAndRxData( data);
	}
	
	public static void s_sendMSearchBroadcast( DatagramSocket s, String service, String host, String content, int port) throws IOException
	{
		String data = "M-SEARCH " + service + USED_PROTOCOL + "Host: " + host + NEWLINE + content;
		s_broadcastData( s, port, data);
	}

	public static List<String> s_getHttpContent( String in)
	{
		String[] strings = in.split("\\r?\\n");
		if (0 >= strings.length) return null;
		if (!strings[0].equalsIgnoreCase("http/1.1 200 ok")) return null;
		return new ArrayList<>(Arrays.asList(strings).subList(1, strings.length));
	}

	//*****************************************************
	// Private methods
	//*****************************************************
	protected String getReply( BinaryReader is, Socket sock) throws IOException, HttpException
	{
		StringBuilder sb = new StringBuilder();
		for (;;)
		{
			String line = is.readLine();
			if (null == line) return sb.toString();
			sb.append( line);
		}
	}
	
	protected String readSpecificLine( BinaryReader is, String expect) throws IOException, HttpException
	{
		for (;;)
		{
			String line = is.readLine();
//			Log.s_logInfo( "Parse line: |" + line + "|");
			if (null == line) throw new HttpException("No content " + expect);
			if (line.startsWith( expect)) return line;
		}
	}
	
	private static void s_broadcastData( DatagramSocket sock, int port, String data) throws IOException
	{
		Log.s_logVerbose("Send broadcast to port " + port + ": '" + data + '\'');
		byte[] bytes = data.getBytes();
		if (!sock.getBroadcast())
		{
			Log.s_logVerbose("Enable broadcast to broadcast-socket");
			sock.setBroadcast(true);
		}
		List<InetAddress> l = HostInfo.getBroadcastAddress();
		for (InetAddress a : l)
		{
			InetSocketAddress broadcastAddr = new InetSocketAddress( a, port);
			DatagramPacket p = new DatagramPacket( bytes, bytes.length, broadcastAddr);
			sock.send( p);
		}
	}

	private String sendAndRxData( String data) throws IOException, HttpException
	{
		Socket sock = new Socket( m_Address, m_Port);
		try
		{
			sock.getOutputStream().write( data.getBytes());
			BinaryReader is = new BinaryReader( sock.getInputStream());
			getResponse( is, sock);
			String res = getReply( is, sock);
			sock.close();
			return res;
		}
		catch( IOException | HttpException e)
		{
			sock.close();
			throw e;
		}
	}

	private void getResponse( BinaryReader is, Socket sock) throws IOException, HttpException
	{
		String ack = is.readLine();
		Log.s_logDebug( "ReadLine: |" + ack + "|");
		Pattern p = Pattern.compile( "^HTTP/1.1\\s+(\\d+)\\s+(.*)\\s*$");
		Matcher m = p.matcher( ack);
		if (m.matches())
		{
			String found = m.group(1);
			int result = Integer.parseInt( found);
			if (result != RESULT_OK)
			{
				String msg = m.group(2);
				throw new HttpException( msg, result);
			}
		}
	}
	
	//*****************************************************
	// Private data members
	//*****************************************************
	private final String			m_Host;
	private final InetAddress 		m_Address;
	private final int				m_Port;
	
	private static final int		RESULT_OK	  = 200;

	private static final String 	USED_PROTOCOL = " http/1.1\r\n";
	private static final String		CONTENT_TEXT  = "CONTENT-TYPE: text/xml; charset=\"utf-8\"\r\n";
	private static final String		NEWLINE	      = "\r\n";
	private static final String		AGENT_CONFIG  = "User-Agent: Guenters_MusicCastApp\r\n" +
												  	"Accept: text/html, application\r\n" +
													"Accept-Language: en-US,en\r\n";
	private static final String		OPT_CLOSECON  = "Connection: Close\r\n";
	private static final int 		DEFAULT_PORT  = 80;
}
