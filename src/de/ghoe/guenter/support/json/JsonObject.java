package de.ghoe.guenter.support.json;

public class JsonObject 
{
	public enum Type { String, Long, Boolean }
	
	public JsonObject( String v, Type t) //throws Exception
	{
		m_Val = v;
		m_Type = t;
	}
	
	public long getIntVal( ) throws Exception
	{
		if (m_Type != Type.Long) throw new Exception( "JSON object is not an integer");
		return Long.parseLong( m_Val);
	}
	
	public boolean getBooleanVal( ) throws Exception
	{
		if (m_Type != Type.Boolean) throw new Exception( "JSON object is not an boolean");
		return Boolean.parseBoolean( m_Val);
	}
	
	public String getStringVal()
	{
		return m_Val;
	}

	private final Type	m_Type;
	private final String	m_Val;
}
