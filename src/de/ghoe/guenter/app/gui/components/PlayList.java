package de.ghoe.guenter.app.gui.components;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Random;

import javax.swing.DefaultListModel;
import javax.swing.JList;

import de.ghoe.guenter.app.gui.IntfApplication;
import de.ghoe.guenter.support.worker.IntfJobProgress;

public class PlayList extends JList<PlayListEntry>
{
	public static PlayList s_createPlayList(IntfApplication app)
	{
		DefaultListModel<PlayListEntry> m = new DefaultListModel<>();
		return new PlayList( app, m);
	}

	public void addEntry( PlayListEntry e)
	{
		m_Model.addElement( e);
	}

	public PlayListEntry[] getEntries()
	{
		int size = m_Model.getSize();
		PlayListEntry[] e = new PlayListEntry[ size];
		for (int i = 0; i < size; ++i)
		{
			e[i] = m_Model.get(i);
		}
		return e;
	}

	public boolean hasEntry( String name)
	{
		for (Enumeration<PlayListEntry> it = m_Model.elements(); it.hasMoreElements();)
		{
			PlayListEntry n = it.nextElement();
			if (name.equals( n.getContent().getName()))
			{
				return true;
			}
		}
		return false;
	}
	
	public boolean removeSelected()
	{
		int[] toRemove = getSelectedIndices();
		int i = toRemove.length;
		if (0 < i)
		{
			do
			{
				m_Model.remove( toRemove[--i]);
			} while( 0 < i);
			return true;
		}
		return false;
	}
	
	public boolean clear()
	{
		if (!m_Model.isEmpty())
		{
			m_Model.clear();
			return true;
		}
		return false;
	}
	
	public boolean moveSelectedUp()
	{
		int[] selected = getSelectedIndices();
		int i = selected.length;
		if ((0 < i) && (0 < selected[0]))
		{
			while( 0 < i--)
			{
				moveElement( selected[i], -1);
			}
			reselectElements( selected, -1);
			return true;
		}
		return false;
	}
	
	public boolean moveSelectedDown()
	{
		int[] selected = getSelectedIndices();
		int i = selected.length;
		if ((0 < i) && (m_Model.getSize()-1 > selected[i-1]))
		{
			while( 0 < i--)
			{
				moveElement( selected[i], 1);
			}
			reselectElements( selected, 1);
			return true;
		}
		return false;
	}
	
	private void reselectElements( int[] e, int off)
	{
		for (int i = 0; i < e.length; ++i)
		{
			e[i] += off;
		}
		this.setSelectedIndices(e);
	}
	
	private void moveElement( int pos, int off)
	{
		PlayListEntry e = m_Model.remove( pos);
		pos += off;
		m_Model.insertElementAt( e, pos);
	}
	
	public boolean randomize()
	{
		int cnt = m_Model.getSize();
		if (1 < cnt)
		{
			Random r = new Random();

			for (int i = 1; i < cnt; ++i)
			{
				int take = r.nextInt(i);
				PlayListEntry e = m_Model.remove(take);
				m_Model.addElement(e);
			}
			return true;
		}
		return false;
	}

	public void load(IntfJobProgress progress, File f) throws IOException
	{
		clear();
		try ( BufferedReader reader = new BufferedReader( new FileReader( f)))
		{
			String line = reader.readLine();
			if (!line.equals( FILE_HEADER)) throw new IOException("Playlist file header is corrupt");
			if (FILE_VERSION != readInt( reader, FILE_HVERSION)) throw new IOException("Playlist file version doesn't match");
			int cnt = readInt( reader, FILE_TRACKCNT);
			progress.progressSetStepCount( cnt);
			while( 0 < cnt--)
			{
				PlayListEntry e = PlayListEntry.s_read( progress, m_IntfApp, reader);
				m_Model.addElement( e);
			}
		}
	}
	
	public void save( File f) throws IOException
	{
		try (BufferedWriter writer = new BufferedWriter( new FileWriter( f)))
		{
			writer.write( FILE_HEADER + '\n');
			writer.write( FILE_HVERSION +  FILE_VERSION + '\n');
			int ecnt = m_Model.size();			
			writer.write( FILE_TRACKCNT + ecnt + '\n');
			for (int i = 0; i < ecnt; ++i)
			{
				m_Model.get(i).save( writer, i);
			}
		}
	}
	
	private int readInt( BufferedReader reader, String header) throws IOException
	{
		String line = reader.readLine();
		if (!line.startsWith( header)) throw new IOException( "Header version not found");
		return Integer.parseInt( line.substring( header.length()));
	}
	
	private PlayList(IntfApplication intfApp, DefaultListModel<PlayListEntry> model)
	{
		super( model);
		m_IntfApp = intfApp;
		m_Model = model;
	}

	private final DefaultListModel<PlayListEntry>	m_Model;
	private final IntfApplication					m_IntfApp;

	private static final int		FILE_VERSION	= 0;
	private static final String		FILE_HEADER		= "# Title=Guenters-Playlist";
	private static final String		FILE_HVERSION	= "Version=";
	private static final String		FILE_TRACKCNT	= "TrackCount=";
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
}
