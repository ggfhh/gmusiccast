package de.ghoe.guenter.support.worker;

import de.ghoe.guenter.support.log.Log;

public abstract class Job extends Thread
{
	public Job(IntfJobProgress progress, String title)
	{
		m_Progress = progress;
		m_Title = title;
	}

	public abstract void doWork( IntfJobProgress progress) throws Exception;

	@Override
	public void run()
	{
		m_Progress.progressStart( m_Title);
		try
		{
			doWork( m_Progress);
		}
		catch (Exception e)
		{
			Log.s_log( "Failed to process " + m_Title, e);
		}
		m_Progress.progressReady();
	}

	protected IntfJobProgress getProgressIf()
	{
		return m_Progress;
	}

	private final IntfJobProgress		m_Progress;
	private final String				m_Title;
}
