package de.ghoe.guenter.support.log;

import java.util.ArrayList;
import java.util.List;

public class Log
{
	public enum Type
	{ 
		Off(0), Error(1), Warning(2), Log(3), Info(4), Verbose(5), Debug(6);
	
		Type( int val) {m_Value = val;}
		public int get() {return m_Value;}
		private final int m_Value;
	}
	
	public static void s_log( Throwable t)
	{
		s_writeLog( Type.Error, "", t.getLocalizedMessage());
	}
	
	public static void s_log( String msg, Throwable t)
	{
		s_writeLog( Type.Error, msg, t.getLocalizedMessage());
	}
	
	public static void s_logError( String msg)
	{
		s_writeLog( Type.Error, msg, null);
	}
	
	public static void s_logWarning( String msg)
	{
		s_writeLog( Type.Warning, msg, null);		
	}
	
	public static void s_logInfo( String msg)
	{
		s_writeLog( Type.Info, msg, null);		
	}
	
	public static void s_logVerbose( String msg)
	{
		s_writeLog( Type.Verbose, msg, null);
	}
	
	public static void s_logDebug( String msg)
	{
		s_writeLog( Type.Debug, msg, null);
	}
	
	public static List<String> s_getEntries()
	{
		List<String> l = s_List;
		s_List = null;
		return l;
	}
	
	private static void s_writeLog( Type t, String msg, String ex)
	{
		if (t.get() <= s_Level.get())
		{
			long time = System.currentTimeMillis();
			if (0 == s_StartTime) s_StartTime = time;
			
			if (null != ex)
			{
				s_doWriteLog( String.format( "%s[%6d] %s -->%s", s_getPrefix(t), time - s_StartTime, msg, ex));
			}
			else
			{
				s_doWriteLog( String.format( "%s[%6d] %s", s_getPrefix(t), time - s_StartTime, msg));
			}
			s_StartTime = time;
		}
	}
	
	public static void s_logHex( Type t, String msg, byte[] data, int cnt)
	{
		s_writeLog( t, msg + " size=" + data.length + " cnt=" + cnt, null);
		for (int i = 0; i < cnt; i += 16)
		{
			StringBuilder sb = new StringBuilder( String.format( "%4s:", i));
			for (int j = 0; j < 16; ++j)
			{
				sb.append( getByteString( data, i+j));
			}
			sb.append( ' ');
			for (int j = 0; (j < 16) && (i+j < cnt); ++j)
			{
				sb.append( (Character.isISOControl(data[i+j])) ? '.' : (char) (data[i+j]));
			}
			s_writeLog( t, sb.toString(), null);
		}
	}
	
	private static String getByteString( byte[] data, int off)
	{
		if (data.length > off)
		{
			int val = data[off];
			if (0 > val) val += 256;
			return String.format(" %02x", val);			
		}
		return "   ";
	}
	
	private static String s_getPrefix( Type t)
	{
		switch(t)
		{
		case Error: return "E";
		case Warning: return "W";
		case Log: return "L";
		case Info: return "I";
		case Verbose: return "V";
		case Debug: return "D";
		default: return "-";
		}
	}
	
	private static synchronized void s_doWriteLog( String message)
	{
		if (null == s_List)
		{
			s_List = new ArrayList<>();
		}
		System.out.println( message);
		s_List.add( message);
	}

	private static long			s_StartTime = 0;
	
//	private static final Type	s_Level = Type.Debug;
//	private static final Type	s_Level = Type.Verbose;
//	private static final Type	s_Level = Type.Info;
//	private static final Type	s_Level = Type.Log;
	private static final Type	s_Level = Type.Warning;
//	private static final Type	s_Level = Type.Error;
//	private static final Type	s_Level = Type.Off;
	private static List<String> s_List;
}
