package de.ghoe.guenter.support.net.support;

import java.net.InetAddress;
import java.net.InterfaceAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import de.ghoe.guenter.support.log.Log;

public class HostInfo
{
	public HostInfo( InetAddress addr, String ipAddrStr, String name)
	{
		m_HostAddress = addr;
		m_HostAddrString = ipAddrStr;
		m_Name     = name;
		m_LastUpdateMs = System.currentTimeMillis();
	}
	
	public boolean isClient()
	{
		return null != m_Name;
	}
	
	public String getName()
	{
		return m_Name;
	}

	public InetAddress getHostAddress() { return m_HostAddress;}
	public String getHostAddrString()
	{
		return m_HostAddrString;
	}
	
	public String getDescription()
	{
		return (null != m_Name) ? m_Name + " (" + m_HostAddrString + ')':
				                  "<not a MusicCast device> (" + m_HostAddrString + ')';
	}
	
	public void update()
	{
		m_LastUpdateMs = System.currentTimeMillis();
	}
	
	public boolean isValid()
	{
		long currTime = System.currentTimeMillis();
		
		return MAX_LIVE_TIME_MS > (currTime - m_LastUpdateMs);
	}
	
	public static List<InetAddress> getBroadcastAddress( )
	{
		ArrayList<InetAddress> list = new ArrayList<>();
		Enumeration<NetworkInterface> interfaces;
		try
		{
			interfaces = NetworkInterface.getNetworkInterfaces();
			while (interfaces.hasMoreElements()) 
			{
			    NetworkInterface networkInterface = interfaces.nextElement();
			    if (networkInterface.isLoopback())
			        continue;    // Do not want to use the loopback interface.
			    for (InterfaceAddress interfaceAddress : networkInterface.getInterfaceAddresses()) 
			    {
			        InetAddress broadcast = interfaceAddress.getBroadcast();
			        if (broadcast == null)
			            continue;
			        list.add( broadcast);
			    }
			}
		}
		catch (SocketException e)
		{
			Log.s_log("Failed to find broadcast addresses", e);
			e.printStackTrace();
		}		
		return list;
	}

	private final InetAddress 	m_HostAddress;
	private final String		m_HostAddrString;
	private final String		m_Name;
	private long   				m_LastUpdateMs;
	
	private static final long MAX_LIVE_TIME_MS = 5000; 
}
