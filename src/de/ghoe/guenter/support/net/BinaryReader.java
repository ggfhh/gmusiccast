package de.ghoe.guenter.support.net;

import java.io.IOException;
import java.io.InputStream;

public class BinaryReader
{
	public BinaryReader( InputStream is)
	{
		m_InputStream = is;
		m_Buf = new byte[0x1000];
		m_BufPos = 0;
		m_BufSize = 0;
		m_LastCharWasCr = false;
	}
	
	public int read( byte[] buffer, int off, int size) throws IOException
	{
		if (m_LastCharWasCr) skippLf();
		int avail = m_BufSize - m_BufPos;
		if (avail >= size)
		{
			System.arraycopy( m_Buf, m_BufPos, buffer, off, size);
			m_BufPos += size;
			return size;
		}
		if (0 < avail)
		{
			System.arraycopy(m_Buf, m_BufPos, buffer, off, avail);
			m_BufPos = 0;
			m_BufSize = 0;
			return avail + m_InputStream.read( buffer, avail, size - avail);
		}
		if (m_LastCharWasCr)
		{
			m_LastCharWasCr = false;
			int cnt = m_InputStream.read( buffer, off, 1);
			if (0 >= cnt) return cnt;
			if (CR != buffer[0]) return m_InputStream.read( buffer, off+1, size-1);
		}
		return m_InputStream.read( buffer, off, size);
	}
	
	public String readLine( ) throws IOException
	{
		if (m_LastCharWasCr) skippLf();
		int pos = findStringTerm( m_BufPos);
		if (0 > pos)
		{	// not found -> fill up buffer
			int start = fillBuffer();
			if (m_LastCharWasCr) skippLf();
			pos = findStringTerm( start);
			if (0 > pos)
			{
				throw new IOException("String is not terminated within buffer");
			}
		}
		String result = new String( m_Buf, m_BufPos, pos-m_BufPos);
		m_BufPos = ++pos;
		return result;			
	}
	
	private int fillBuffer( ) throws IOException
	{ // return count of old data
		int cnt = m_BufSize - m_BufPos;
		System.arraycopy(m_Buf, m_BufPos, m_Buf, 0, cnt);
		m_BufPos = 0;
		int space = m_Buf.length - cnt;
		int rcnt = m_InputStream.read( m_Buf, cnt, space);
		m_BufSize = (0 < rcnt) ? cnt + rcnt : cnt;
		return cnt;
	}
	
	private int findStringTerm( int start)
	{
		for (int i = start; i < m_BufSize; ++i)
		{
			byte b = m_Buf[i];
			switch( b)
			{
			case CR:
				m_LastCharWasCr = true;
			case LF:
			case ST:
				return i;
			default:
				break;
			}
		}
		return -1;		
	}
	
	private void skippLf()
	{
		if (m_BufPos < m_BufSize)
		{
			if (LF == m_Buf[m_BufPos])
			{
				++m_BufPos;
			}
			m_LastCharWasCr = false;
		}
	}

	private final byte[]		m_Buf;
	boolean						m_LastCharWasCr;
	int							m_BufPos;
	int							m_BufSize;
	private final InputStream	m_InputStream;
	
	private static final byte	CR	= 0x0d;
	private static final byte	LF	= 0x0a;
	private static final byte	ST	= 0x00;
	
}
