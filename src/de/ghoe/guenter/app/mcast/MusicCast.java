package de.ghoe.guenter.app.mcast;

import java.net.InetAddress;
import java.util.Vector;

import de.ghoe.guenter.app.gui.GMusicCastGui;
import de.ghoe.guenter.app.gui.IntfApplication;
import de.ghoe.guenter.app.mcast.cmd.Cmd;
import de.ghoe.guenter.app.mcast.cmd.CmdException;
import de.ghoe.guenter.app.mcast.cmd.netusb.*;
import de.ghoe.guenter.app.mcast.cmd.sys.Cmd_GetNetworkStatus;
import de.ghoe.guenter.app.mcast.data.ContentNode;
import de.ghoe.guenter.app.mcast.data.PlayListInfo;
import de.ghoe.guenter.support.json.Json;
import de.ghoe.guenter.support.json.JsonHash;
import de.ghoe.guenter.support.log.Log;
import de.ghoe.guenter.support.net.NetHttpJson;

public class MusicCast 
{
	//*****************************************************
	// Constructors
	//*****************************************************
	public MusicCast(IntfApplication app, InetAddress addr)
	{
		m_App = app;
		m_HostName = addr.getHostName();
		m_NetHttp = new NetHttpJson( addr);
		m_Content = new ContentNode(app, null, "Server", 0, false);
		m_playListInfo = null;
		selectRoot();
	}
	
	//*****************************************************
	// Public methods
	//*****************************************************	
	public boolean isHost( String hostName)
	{
		return m_HostName.equals( hostName);
	}
	
	public ContentNode getContent()
	{
		return m_Content;
	}

	public ContentNode getContent(String[] path)
	{
		ContentNode cn = m_Content;
		for (String name : path)
		{
			Log.s_logVerbose( "Get node " + name);
			cn = cn.getChild( name);
			if (null == cn) break;
		}
		return cn;
	}

	public String[] getPlayListNames()
	{
		if (null == m_playListInfo)
		{
			m_playListInfo = new PlayListInfo(m_App);
		}
		return m_playListInfo.getPlaylistNames();
	}

	public ContentNode findContent( String content)
	{
		return m_Content.findContent( content);
	}

	public String[] readPlayList( int idx) throws Exception
	{
		Log.s_logVerbose( "Read playlist " + idx);
		Cmd_GetMcPlaylist cmd = new Cmd_GetMcPlaylist( idx);
		m_App.executeCommand( cmd);
		return cmd.getNames();
	}

	public void renamePlayList(int playListIndex, String newName) throws Exception
	{
		Log.s_logInfo("Rename playlist " + playListIndex + " to '" + newName + '\'');
		Cmd_SetMcPlaylistName cmd = new Cmd_SetMcPlaylistName( playListIndex, newName);
		executeCommand( cmd, "Failed to rename playlist " + playListIndex + " to 'playlist '" + newName + '\'');
	}

	public void clearPlayList(int playListIndex) throws Exception
	{
		Log.s_logInfo("Clear playlist " + playListIndex);
		Cmd_ClearMcPlaylist cmd = new Cmd_ClearMcPlaylist( playListIndex);
		executeCommand( cmd, "Failed to clear playlist " + playListIndex);
	}

	public void addNodeToPlaylist(int playListIndex, int entryIdx) throws Exception
	{
		Log.s_logVerbose("Add track-index " + entryIdx + " to playlist " + playListIndex);
		Cmd_ManageList cmd = new Cmd_ManageList(Cmd_ManageList.Type.AddToPlayList, entryIdx, playListIndex);
		executeCommand( cmd, "Failed to add track " + entryIdx + " to playlist " + playListIndex);
	}

	public Vector<String> getCommands( )
	{
		return Cmd.s_getCommandNames();
	}
	
	public static String s_getCommandDescription()
	{
		return Cmd.s_getCommandDescription();
	}

	public JsonHash executeCommand( Cmd cmd) throws Exception
	{
		cmd.execute( m_NetHttp);
		return cmd.getResult();
	}
	
	public void executeCommand( Cmd cmd, String errorMsg) throws Exception
	{
		cmd.execute( m_NetHttp);
		if  (!cmd.checkResponse())
		{
			throw new CmdException( errorMsg + cmd.getErrorString());
		}
	}

	public String executeCommand( String cmd, String param, GMusicCastGui gui) throws Exception
	{
		Log.s_logInfo( "Execute command " + cmd + ": " + param);
		
		Cmd command = Cmd.s_createCommand( cmd, param);
		command.execute( m_NetHttp);
		String errorResponse = command.getErrorString();
		if (null == errorResponse)
		{
			Json result = command.getResult();
			gui.setReply(cmd, result);
			Log.s_logInfo( "Reply: " + result.getText(""));
		}
		else
		{
			Log.s_logError( "Failed to execute command: '" + cmd + "' response: " + errorResponse);
		}
		return errorResponse;
	}

	public static String s_getHostName(IntfApplication app, InetAddress ip)
	{
		try
		{
			MusicCast tempMC = new MusicCast( app, ip);
			Cmd_GetNetworkStatus cmd = new Cmd_GetNetworkStatus(null);
			cmd.execute( tempMC.m_NetHttp);
			return cmd.getDeviceName();
		}
		catch ( Throwable e)
		{
			Log.s_logInfo( "IP " + ip + " is not a MusicCast device");
		}
		return null;
	}
	
	//*****************************************************
	// Private methods
	//*****************************************************
	private void selectRoot()
	{
		try
		{
			Cmd cmd = new Cmd_SetListControl( Cmd_SetListControl.Op.Return);
			do
			{
				cmd.execute(m_NetHttp);
			} while( 0 == cmd.getCommandResponse());
			if (4 != cmd.getCommandResponse())
			{
				Log.s_logError("Failed to enter root-path. Reply=" + cmd.getErrorString());
			}
		}
		catch (Exception e)
		{
			Log.s_log("Failed to enter root-directory", e);
		}
	}

	//*****************************************************
	// Private data
	//*****************************************************
	private final IntfApplication 						m_App;
	private final NetHttpJson							m_NetHttp;
	private final String								m_HostName;
	private final ContentNode							m_Content;
	private PlayListInfo								m_playListInfo;
}
