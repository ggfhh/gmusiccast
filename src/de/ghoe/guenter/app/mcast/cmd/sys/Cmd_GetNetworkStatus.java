package de.ghoe.guenter.app.mcast.cmd.sys;

import de.ghoe.guenter.app.mcast.cmd.CmdException;
import de.ghoe.guenter.support.json.JsonHash;
import de.ghoe.guenter.support.json.JsonString;

public class Cmd_GetNetworkStatus extends CmdSys
{
	public Cmd_GetNetworkStatus( String param)
	{		
	}

	@Override
	public String getCmd()
	{
		return CMD_NAME;
	}
	
	public String getDeviceName() throws CmdException
	{
		JsonHash j = getResult();
		JsonString name = j.getJasonString( "network_name");
		return name.getString();
	}

	public static final String CMD_NAME = "getNetworkStatus";
	public static final String CMD_DESC = "Read network status information";
}

