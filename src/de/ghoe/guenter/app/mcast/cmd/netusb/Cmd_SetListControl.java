package de.ghoe.guenter.app.mcast.cmd.netusb;

import de.ghoe.guenter.app.mcast.cmd.CmdException;

public class Cmd_SetListControl extends CmdNetUsb
{
	public enum Op { Select, Play, Return}
	
	public Cmd_SetListControl( String parameter) throws CmdException
	{
		StringBuilder sb = new StringBuilder( CMD_NAME);
		String[] data = parameter.split( "\\s+");
		switch( data.length)
		{
		case 1: 
			sb.append( "?type=");
			sb.append( data[0]);
			break;
		case 2:
			sb.append( "?type=");
			sb.append( data[0]);
			sb.append( "&index=");
			sb.append( data[1]);
			break;
		default:
			throw new CmdException( CMD_NAME + " used with invalid count of parameter (1 or 2)");
		}
		m_Cmd = sb.toString();
	}
	
	public Cmd_SetListControl( Op op) throws CmdException
	{
		StringBuilder sb = createCmd( op);
		m_Cmd = sb.toString();
	}
	
	public Cmd_SetListControl( Op op, int off) throws CmdException
	{
		StringBuilder sb = createCmd( op);
		sb.append( "&index=");
		sb.append( off);
		m_Cmd = sb.toString();
	}

	@Override
	public String getCmd()
	{
		return m_Cmd;
	}

	private StringBuilder createCmd( Op t) throws CmdException
	{
		StringBuilder sb = new StringBuilder( CMD_NAME);
		sb.append( "?type=");
		switch( t)
		{
		case Select:
			sb.append( "select");
			break;
		case Play:
			sb.append( "play");
			break;
		case Return:
			sb.append( "return");
			break;
		default:
			throw new CmdException( CMD_NAME + " invalid operation used");
		}
		return sb;
	}
	
	private final String	m_Cmd;

	public static final String CMD_NAME = "setListControl";
	public static final String CMD_DESC = "Set operation on current list";
}

