package de.ghoe.guenter.support.net;

import java.net.InetAddress;

public interface IntfNetSsdpReceive
{
	void rxSsdpHostUpdate( InetAddress ipAddr);
}
