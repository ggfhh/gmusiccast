package de.ghoe.guenter.app.mcast.cmd.sys;

import de.ghoe.guenter.app.mcast.cmd.Cmd;

public abstract class CmdSys extends Cmd
{
	protected CmdSys()
	{
		
	}
	
	public String getCmdGroup()
	{
		return "system"; 
	}
	
	public abstract String getCmd();
	
	public static void s_addCommands( StringBuilder sb)
	{
		s_addCommand( sb, Cmd_GetDeviceInfo.class);
		s_addCommand( sb, Cmd_GetFeatures.class);
		s_addCommand( sb, Cmd_GetLocationInfo.class);
		s_addCommand( sb, Cmd_GetNameText.class);
		s_addCommand( sb, Cmd_GetNetworkStatus.class);
	}
}
