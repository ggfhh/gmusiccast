package de.ghoe.guenter.app.mcast.jobs;

import de.ghoe.guenter.app.gui.components.PlayListEntry;
import de.ghoe.guenter.support.log.Log;
import de.ghoe.guenter.support.worker.IntfJobProgress;
import de.ghoe.guenter.support.worker.Job;

public class JobUpdatePlaylist extends Job
{
	public JobUpdatePlaylist(IntfJobProgress progress, String title, PlayListEntry[] list, int playlistIndex)
	{
		super(progress, title);
		m_List = list;
		m_FailedEntries = null;
		m_PlaylistIndex = playlistIndex;
		start();
	}

	public String getFailedList()
	{
		return (null != m_FailedEntries) ? m_FailedEntries.toString() : null;
	}

	@Override
	public void doWork(IntfJobProgress progress)
	{
		progress.progressSetStepCount( m_List.length);
		for (PlayListEntry e : m_List)
		{
			String path = e.getContent().getPathAsString();
			progress.progressNextStep("Add: " + path);
			Log.s_logVerbose("Add Track to PlayList " + m_PlaylistIndex + ": " + path);
			if (!e.getContent().addToPlaylist(m_PlaylistIndex))
			{
				if (null == m_FailedEntries) m_FailedEntries = new StringBuilder("List of tracks not added:");
				m_FailedEntries.append("\nTrack ");
				m_FailedEntries.append(m_PlaylistIndex);
				m_FailedEntries.append('=');
				m_FailedEntries.append(e.getContent().getPathAsString());
			}
		}
	}

	private final PlayListEntry[] 	m_List;
	private StringBuilder			m_FailedEntries;
	private final int 				m_PlaylistIndex;
}
