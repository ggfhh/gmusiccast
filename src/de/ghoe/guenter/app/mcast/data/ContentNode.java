package de.ghoe.guenter.app.mcast.data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;

import javax.swing.tree.TreeNode;

import de.ghoe.guenter.app.gui.IntfApplication;
import de.ghoe.guenter.support.log.Log;

public class ContentNode implements TreeNode
{
	public ContentNode(IntfApplication app, ContentNode parent, String name, int index, boolean isLeaf)
	{
		m_App = app;
		m_Name = name;
		m_Index = index;
		m_Parent = parent;
		m_Content = null;
		m_IsLeaf = isLeaf;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////
	// public methods
	public String toString()
	{
		return m_Name;
	}

	public ContentNode findContent( String content)
	{
		Log.s_logVerbose( "Search '" + content + "' in entry '" + m_Name + '\'');
		if (this.m_Name.equals( content)) return this;

		if (loadChildren())
		{
			for (ContentNode cn : m_Content)
			{
				ContentNode res = cn.findContent(content);
				if (null != res) return res;
			}
		}
		return null;
	}

	public ContentNode getChild(String name)
	{
		Log.s_logVerbose( "Find '" + name + "' within node '" + m_Name + ':');
		if (loadChildren())
		{
			for (ContentNode cn : m_Content)
			{
				Log.s_logVerbose( "\tCheck '" + cn.m_Name + '\'');
				if (cn.m_Name.equals( name)) return cn;
			}
		}
		return null;
	}

	public List<Integer> getPositionPath()
	{
		if (null == m_Parent) return new ArrayList<>();
		List<Integer> list = m_Parent.getPositionPath();
		list.add( m_Index);
		return list;
	}
	
	public List<String> getStringPath()
	{
		if (null == m_Parent) return new ArrayList<>();
		List<String> list = m_Parent.getStringPath();
		list.add( m_Name);
		return list;
	}

	public String getName()
	{
		return m_Name;
	}

	public String getExtendedName()
	{
		if (null == m_Parent) return m_Name;
		return m_Name + " (" + m_Parent.getName() + ')';
	}

	public String getPathAsString()
	{
		List<Integer> li = getPositionPath();
		List<String>  ls = getStringPath();

		int ci = li.size();
		int cs = ls.size();
		
		if (ci != cs)
		{
			Log.s_logError("Path differ at index (" + ci + ") and string (" + cs +")");
			if (cs > ci) cs = ci;
		}
		StringBuilder sb = new StringBuilder( (isLeaf() ? "IsLeaf [" : "IsNode ["));
		sb.append(cs);
		sb.append( "]:");
		for (int i = 0; i < cs; ++i)
		{
			sb.append( " -> ");
			sb.append( li.get(i));
			sb.append( ":");
			sb.append( ls.get(i));
		}
		return sb.toString();
	}

	public boolean addToPlaylist( int playListIndex)
	{
		boolean res;
		if (loadChildren())
		{	// need to add all
			res = true;
			for (ContentNode n : m_Content)
			{
				res = n.addToPlaylist( playListIndex) && res;
			}
		}
		else
		{	// last node -> add me :-)
			res = m_App.addNodeToPlaylist( playListIndex, getPositionPath());
		}
		return res;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////
	// Tree Node interface
	@Override
	public TreeNode getChildAt(int childIndex)
	{
		if (loadChildren() && (childIndex < m_Content.size()))
		{
			return m_Content.get( childIndex);
		}
		return null;
	}

	@Override
	public int getChildCount()
	{
		if (loadChildren())
		{
			return m_Content.size();
		}
		return 0;
	}

	@Override
	public TreeNode getParent()
	{
		return m_Parent;
	}

	@Override
	public int getIndex(TreeNode node)
	{
		int idx = loadChildren() ? m_Content.size() : 0;
		while( 0 < idx--)
		{
			TreeNode v = m_Content.get( idx);
			if (v == node) break;
		}
		return idx;
	}

	@Override
	public boolean getAllowsChildren()
	{
		return !m_IsLeaf;
	}

	@Override
	public boolean isLeaf()
	{
		return m_IsLeaf;
	}

	@Override
	public Enumeration<? extends TreeNode> children()
	{
		if (loadChildren())
		{
			return Collections.enumeration( m_Content);
		}
		// TODO Auto-generated method stub
		return null;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////
	// private methods
	private boolean loadChildren( )
	{
		if (!m_IsLeaf && (null == m_Content))
		{
			m_Content = m_App.readFromPath( this.getPositionPath(), this);
			return null != m_Content;
		}
		return !m_IsLeaf;
	}
	
	///////////////////////////////////////////////////////////////////////////////////////////////
	// private data
	private final IntfApplication		m_App;
	private final String				m_Name;
	private final int					m_Index;
	private final boolean				m_IsLeaf;
	private final ContentNode			m_Parent;
	private List<ContentNode>			m_Content;
}
