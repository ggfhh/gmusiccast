/*
 * Application to handle MusicCast devices
 */
package de.ghoe.guenter.app;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;


import de.ghoe.guenter.app.gui.GMusicCastGui;
import de.ghoe.guenter.app.gui.IntfApplication;
import de.ghoe.guenter.app.mcast.MusicCast;
import de.ghoe.guenter.app.mcast.cmd.Cmd;
import de.ghoe.guenter.app.mcast.cmd.CmdException;
import de.ghoe.guenter.app.mcast.cmd.netusb.Cmd_GetListInfo;
import de.ghoe.guenter.app.mcast.cmd.netusb.Cmd_SetListControl;
import de.ghoe.guenter.app.mcast.data.ContentNode;
import de.ghoe.guenter.support.config.UserConfig;
import de.ghoe.guenter.support.json.JsonArray;
import de.ghoe.guenter.support.json.JsonHash;
import de.ghoe.guenter.support.log.Log;
import de.ghoe.guenter.support.net.IntfNetSsdpReceive;
import de.ghoe.guenter.support.net.NetSsdp;
import de.ghoe.guenter.support.net.support.HostInfo;

/**
 * @author guenter
 *
 */
public class GMusicCast implements IntfNetSsdpReceive, IntfApplication
{
	public GMusicCast( )
	{
		m_MusicCast = null;
		m_Hosts = new HashMap<>();
		m_Gui = new GMusicCastGui( "Guenters MusicCast Application", VERSION, this);
		m_DeviceFinder = new NetSsdp( this);
		m_DeviceFinder.broadcastDiscovery();
	}
	
	//*****************************************************
	// IntfNetSsdpReceive
	//*****************************************************
	@Override
	public void rxSsdpHostUpdate(InetAddress ipAddr)
	{
		String ipAddStr = ipAddr.getHostAddress();
		Log.s_logInfo("Got SSDP-packet: " + ipAddStr);
		HostInfo hi = m_Hosts.get( ipAddStr);
		if (null == hi)
		{
			Log.s_logInfo("SSDP-packet new Address: " + ipAddStr);
			String name = MusicCast.s_getHostName(this, ipAddr);
			hi = new HostInfo( ipAddr, ipAddStr, name);
			m_Hosts.put( ipAddStr, hi);
			if (null != name)
			{
				Log.s_logVerbose("Update DeviceList");
				m_Gui.updateDeviceList( getDeviceList());				
			}
		}
	}
	
	//*****************************************************
	// IntfApplication
	//*****************************************************
	private boolean closeDevice( String ip)
	{
		if (null == m_MusicCast) return (null != ip);
		if (m_MusicCast.isHost(ip)) return false;		
		m_MusicCast = null;
		return true;
	}
	
	@Override
	public void selectDevice(String deviceIpStr)
	{
		if (closeDevice( deviceIpStr))
		{
			Log.s_logInfo( "Open Device: " + deviceIpStr);
			HostInfo info = m_Hosts.get( deviceIpStr);
			if (null != info)
			{
				m_MusicCast = new MusicCast(this, info.getHostAddress());
				m_Gui.notifyDeviceChange();
			}
		}
	}
	
	@Override
	public Vector<String> getCommands()
	{
		return Cmd.s_getCommandNames();
	}
	
	@Override
	public void executeCommand(String command, String param)
	{
		if (null != m_MusicCast)
		{
			try
			{
				String result = m_MusicCast.executeCommand( command, param, m_Gui);
				m_Gui.setState( (null != result) ? ("Result: " + result): "Result: Success");
			}
			catch (Exception e)
			{
				m_Gui.setState( "State command " + command + " failed: " + e.getLocalizedMessage());
				Log.s_log( "Failed to execute command " + command, e);
			}
		}
	}

	@Override
	public JsonHash executeCommand(Cmd cmd) throws Exception
	{
		if (null == m_MusicCast) throw new CmdException( "Not Connected");
		return m_MusicCast.executeCommand(cmd);
	}

	@Override
	public String getCommandDescription()
	{
		return MusicCast.s_getCommandDescription();
	}

	@Override
	public String[] getPlayListNames()
	{
		return (null != m_MusicCast) ? m_MusicCast.getPlayListNames() : null;
	}

	@Override
	public String[] getPlayListContent(int listIdx, String listName) throws Exception
	{
		Log.s_logInfo("Read Playlist " + listIdx + ": " + listName);
		return (null != m_MusicCast) ? m_MusicCast.readPlayList( listIdx) : new String[0];
	}

	@Override
	public ContentNode findContent(String name)
	{
		Log.s_logInfo("Find content entry '" + name + "'");

		return (null != m_MusicCast) ? m_MusicCast.findContent( name) : null;
	}

	@Override
	public boolean renamePlayList(int playListIndex, String newName)
	{
		if (null != m_MusicCast)
		{
			try
			{
				m_MusicCast.renamePlayList( playListIndex, newName);
				return true;
			}
			catch (Exception e)
			{
				Log.s_log("Failed to rename playlist", e);
			}
		}
		return false;
	}

	@Override
	public boolean clearPlayList(int playListIndex)
	{
		if (null != m_MusicCast)
		{
			try
			{
				m_MusicCast.clearPlayList( playListIndex);
				return true;
			}
			catch (Exception e)
			{
				Log.s_log("Failed to clear playlist", e);
			}
		}
		return false;
	}

	@Override
	public boolean addNodeToPlaylist(int playListIndex, List<Integer> path)
	{
		if (null != m_MusicCast)
		{
			try
			{
				int idx = path.remove( path.size()-1);	// last path value is entry-index to add
				changeCurrentPath( path);
				m_MusicCast.addNodeToPlaylist( playListIndex, idx);
				return true;
			}
			catch(Exception e)
			{
				Log.s_log("Failed to add track to playlist " + playListIndex, e);
			}
		}
		return false;
	}

	@Override
	public ContentNode getContent()
	{
		return (null != m_MusicCast) ? m_MusicCast.getContent() : null;
	}

	@Override
	public ContentNode getContent(String[] path) { return (null != m_MusicCast) ? m_MusicCast.getContent( path) : null; }

	@Override
	public List<ContentNode> readFromPath(List<Integer> path, ContentNode parent)
	{
		Log.s_logVerbose( "ReadFromPath: " + parent.getPathAsString());
		if (null != m_MusicCast)
		{
			try
			{
				List<ContentNode> l = new ArrayList<>();
				Cmd_GetListInfo currCmd = changeCurrentPath( path);
				readRemainingEntries( l, parent, currCmd);
				return l;
			}
			catch (Exception e)
			{

				Log.s_log("Failed to execute setPath", e);
			}
		}
		return null;
	}
	
	@Override
	public void sendDeviceDiscover()
	{
		m_DeviceFinder.broadcastDiscovery();
	}
	
	//*****************************************************
	// public member
	//*****************************************************
	/**
	 * @param args unused
	 */
	public static void main(String[] args)
	{
		Runtime.getRuntime().addShutdownHook(new Thread(GMusicCast::doShutdown));
		GMusicCast app = new GMusicCast();
		app.m_Gui.setVisible( true);
	}

	private static void doShutdown()
	{
		UserConfig.s_save();
	}

	//*****************************************************
	// private member
	//*****************************************************
	private Vector<String> getDeviceList()
	{
		Vector<String> v = new Vector<>();
		for (HostInfo hi : m_Hosts.values())
		{
			if (hi.isClient())
			{
				v.add( hi.getDescription());
			}
		}
		return v;
	}

	private int goFolderUpExecute() throws Exception
	{
		Cmd_SetListControl cmd = new Cmd_SetListControl( Cmd_SetListControl.Op.Return);
		m_MusicCast.executeCommand(cmd);
		return cmd.getCommandResponse();
	}

	private void goFolderUp( ) throws Exception
	{
		int res = goFolderUpExecute();
		switch( res)
		{
			case 0:	// success -> continue
				m_CurrentPath.remove( m_CurrentPath.size()-1); // remove last entry
				break;
			case 4: // failed already in root
				Log.s_logWarning("Failed to go folder up -> already at root.");
				m_CurrentPath.clear();
				return;
			default:
				throw new CmdException( "Failed to set path to root. " + Cmd.s_getErrorString(res));
		}
	}

	private void setToCommonPath( List<Integer> newPath) throws Exception
	{
		Log.s_logVerbose("Set to common path from: " + getCurrentPathString());
		Log.s_logVerbose("                     to: " + s_getIntVectorString( newPath));

		if (null != m_CurrentPath)
		{
			int cs = m_CurrentPath.size();
			int ns = newPath.size();
			if (ns > cs) ns = cs;
			for (int i = 0; i < ns; ++i)
			{
				if (!m_CurrentPath.get(i).equals(newPath.get(i)))
				{
					ns = i;
				}
			}
			for (int i = ns; i < cs; ++i)
			{
				goFolderUp();
			}
			Log.s_logVerbose("Common = " + getCurrentPathString());
		}
		setToRoot();
	}

	private Cmd_GetListInfo changeCurrentPath( List<Integer> path) throws Exception
	{
		try
		{
			setToCommonPath(path);
			Log.s_logInfo("Set path to: " + s_getIntVectorString( path));
			int start = m_CurrentPath.size();
			Cmd_GetListInfo cmdInfo = new Cmd_GetListInfo("server", 0);
			for (int i = start; i < path.size(); ++i)
			{
				int pos = path.get(i);
				Log.s_logInfo("\tChoose: " + path);
				Cmd_SetListControl cmd = new Cmd_SetListControl(Cmd_SetListControl.Op.Select, pos);
				m_MusicCast.executeCommand(cmdInfo, "Failed to get info about current directory");
				m_MusicCast.executeCommand(cmd, "Failed to select given path: ");
				m_CurrentPath.add(pos);
			}
			Log.s_logVerbose( "Set path (to  : " + getCurrentPathString() + ")");
			m_MusicCast.executeCommand(cmdInfo, "Failed to get info about current directory");
			return cmdInfo;
		}
		catch (Throwable e)
		{
			m_CurrentPath = null;
			throw e;
		}
	}

	private void setToRoot()
	{
		m_CurrentPath = new Vector<>();
		try
		{
			//noinspection StatementWithEmptyBody
			while (0 == goFolderUpExecute()) ;
		}
		catch(Throwable e)
		{
			Log.s_log("Failed to enter root-path", e);
		}
	}

	private void readRemainingEntries( List<ContentNode> l, ContentNode parent, Cmd_GetListInfo firstCmd) throws Exception
	{
		JsonHash cmdRes = firstCmd.getResult();
		int cnt = cmdRes.getJasonNumber("max_line").getIntegerValue();
		Log.s_logInfo("Read content of " + cnt + " entries");
		fillDataFromResult( l, cmdRes.getJasonArray("list_info"), parent);
		Log.s_logInfo("\tInitial : " + l.size() + " entries");
		while( l.size() < cnt)
		{
			Cmd_GetListInfo cmd = new Cmd_GetListInfo( "server", l.size());
			m_MusicCast.executeCommand(cmd, "Failed to get content starting from " + l.size());
			fillDataFromResult( l, cmd.getResult().getJasonArray("list_info"), parent);
			Log.s_logInfo("\tContinue: " + l.size() + " entries");
		}
	}

	private void fillDataFromResult( List<ContentNode> l, JsonArray entries, ContentNode parent) throws Exception
	{
		int cnt = entries.getEntryCount();
		for (int i = 0; i < cnt; ++i)
		{
			JsonHash entry = entries.getJasonHash(i);
			String title = entry.getJasonString("text").getString();				
			boolean isLeaf = checkForType( entry, l.size());
			ContentNode cn = new ContentNode( this, parent, title, l.size(), isLeaf);
			l.add( cn);			
		}		
	}
		
	private boolean checkForType( JsonHash e, int pos) throws Exception
	{
		int type = e.getJasonNumber("attribute").getIntegerValue();		
		
		switch( type)
		{
		case 0x00000002:	// server main entry
			Log.s_logVerbose( "Got Server entry " + pos + ": " + e.getJasonString("text").getString());
			return false;
		case 0x07800002:	// Root folders
			Log.s_logVerbose( "Got Type-Folder " + pos + ": " + e.getJasonString("text").getString());
			return false;
		case 0x07800004:	// File
			Log.s_logVerbose( "Got File-Entry " + pos + ": " + e.getJasonString("text").getString());
			return true;
		default:
			Log.s_logWarning( "Got unknown entry " + pos + ": " + Integer.toHexString( type) + " for entry: " + e.getJasonString("text").getString());
			return true;
		}
	}

	private static String s_getIntVectorString( List<Integer> l)
	{
		StringBuilder sb = new StringBuilder( "root:");
		if (null != l)
		{
			for (Integer i : l)
			{
				sb.append( " ");
				sb.append( i);
			}
		}
		return sb.toString();
	}

	private String getCurrentPathString()
	{
		return s_getIntVectorString( m_CurrentPath);
	}

	//*****************************************************
	// private data
	//*****************************************************	
	/**
	 * Private data for class
	 */
	private final GMusicCastGui				m_Gui;
	private MusicCast						m_MusicCast;
	private final NetSsdp					m_DeviceFinder;
	private final HashMap<String,HostInfo>	m_Hosts;

	private  Vector<Integer>				m_CurrentPath;

	private final static String		VERSION = "0.0.6";
}
