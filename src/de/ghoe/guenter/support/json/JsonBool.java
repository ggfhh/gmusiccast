package de.ghoe.guenter.support.json;

public class JsonBool extends Json
{
	public JsonBool( String name, boolean val)
	{
		super(name);
		m_Val = val;
	}
	
	@Override
	public Object getObject()
	{
		return m_Val;
	}

	@Override
	public String getString( )
	{
		return m_Val ? "true" : "false";
	}
	
	public boolean getValue()
	{
		return m_Val;
	}

	private final Boolean	m_Val;
}
