package de.ghoe.guenter.app.mcast.cmd.sys;

public class Cmd_GetLocationInfo extends CmdSys
{
	public Cmd_GetLocationInfo( String param)
	{		
	}
	
	@Override
	public String getCmd()
	{
		return CMD_NAME;
	}

	public static final String CMD_NAME = "getLocationInfo";
	public static final String CMD_DESC = "Read location information from device";
}
