package de.ghoe.guenter.app.gui.components;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.JComboBox;
import javax.swing.JPanel;

import de.ghoe.guenter.app.gui.IntfApplication;
import de.ghoe.guenter.app.gui.components.support.ResizeComponent;

public class DeviceListGui extends JPanel implements ActionListener
{
	public DeviceListGui( IntfApplication intfApp)
	{
		m_AppIf = intfApp;
		setLayout( new FlowLayout());
		m_DeviceList = new JComboBox<>();
		ResizeComponent.s_setWidth(m_DeviceList, 240);
		m_DeviceList.addActionListener( this);
		add( m_DeviceList);
		setCurrentDevice();
	}

	public void update(Vector<String> devs)
	{
		Object cs = m_DeviceList.getSelectedItem();
		String currSel = (null != cs) ? cs.toString() : "";
		
		m_DeviceList.removeAllItems();
		int selIdx = 0;
		
		for (int idx = 0; idx < devs.size(); ++idx)
		{
			String s = devs.elementAt( idx);
			m_DeviceList.insertItemAt( s, idx);
			if (s.equals(currSel))
			{
				selIdx = idx;
			}
		}
		if (selIdx < m_DeviceList.getItemCount())
		{
			m_DeviceList.setSelectedIndex( selIdx);
		}
		setCurrentDevice();
	}
	
	@Override
	public void actionPerformed(ActionEvent evnt)
	{
		setCurrentDevice();
	}
	
	private void setCurrentDevice()
	{
		Object o = m_DeviceList.getSelectedItem();
		if (null != o)
		{
			String s = o.toString();
			String addr = s.replaceFirst( "^.*\\(","");
			addr = addr.replaceFirst( "\\).*$", "");
			m_AppIf.selectDevice( addr);
		}
	}
	
	/**
	 * 
	 */
	private final IntfApplication		m_AppIf;
	private final JComboBox<String>	m_DeviceList;

	private static final long serialVersionUID = 1L;

}
