package de.ghoe.guenter.support.net;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;

import de.ghoe.guenter.support.json.Json;
import de.ghoe.guenter.support.json.JsonHash;
import de.ghoe.guenter.support.log.Log;

public class NetHttpJson extends NetHttp
{
	public NetHttpJson(InetAddress addr)
	{
		super(addr);
	}
	public NetHttpJson(InetAddress addr, int port)
	{
		super(addr, port);
	}
	
	public JsonHash getJson( String request) throws Exception
	{
		String in = sendGet(request);
		return Json.s_createObject(in);
	}

	public JsonHash postJson( String request, String data) throws Exception
	{
		String in = sendPost(request, data);
		return Json.s_createObject(in);
	}

	//*****************************************************
	// Private methods
	//*****************************************************
	protected String getReply( BinaryReader is, Socket sock) throws IOException, HttpException
	{
		String line = readSpecificLine( is, "Content-Type:");
		if (!line.endsWith( "application/json")) throw new HttpException("Unexpected content type"); 
		line = readSpecificLine( is, "Content-Length:");
		String b = line.replaceFirst( ".*:\\s*","");
		int bytes = Integer.parseInt( b);
		// now read until empty line detected
		do
		{
			line = is.readLine();
			Log.s_logVerbose( "\tRead until empty line: '" + line + "' length=" + line.length());
		} while( !line.isEmpty());
		byte[] buf = new byte[bytes];
		int rbytes = 0;
		while( rbytes < buf.length)
		{
			Log.s_logVerbose("Json read " + rbytes + " of " + bytes );
			int r = is.read(buf, rbytes, bytes - rbytes);
			Log.s_logHex( Log.Type.Verbose, "JSon received", buf, rbytes + r);
			if (0 >= r) break;
			rbytes += r;
		}
		if (rbytes != bytes) Log.s_logHex( Log.Type.Verbose, "Read: ", buf, rbytes);
		if (rbytes != bytes) throw new IOException( "Failed to read all data (read=" + rbytes + ", expect=" + bytes +")");
		String reply = new String( buf); 
		Log.s_logDebug( "Read " + bytes + ": " + reply);
		return reply;
	}
}

