package de.ghoe.guenter.app.gui;

import de.ghoe.guenter.app.gui.components.support.ResizeComponent;
import de.ghoe.guenter.support.worker.IntfJobProgress;

import javax.swing.*;
import java.awt.*;

public class ProgressInfo extends JDialog implements IntfJobProgress
{
	public ProgressInfo( Window parent, int width)
	{
		super( parent, "Progress-Info", ModalityType.APPLICATION_MODAL);
		this.setLayout( new BorderLayout());
		m_Progress = new JProgressBar( );
		m_Progress.setStringPainted(true);
		ResizeComponent.s_setWidth( m_Progress, width);
		add( m_Progress, BorderLayout.NORTH);
		m_Text = new JTextField("");
		m_Text.setEditable( false);
		ResizeComponent.s_setWidth( m_Text, width);
		add( m_Text, BorderLayout.SOUTH);
		pack();
	}

	@Override
	public void progressStart( String title)
	{
		if (SwingUtilities.isEventDispatchThread())
		{
			this.setTitle( title);
			setVisible( true);
		}
		else
		{
			SwingUtilities.invokeLater(() -> p_start( title));
		}
	}

	@Override
	public void progressSetStepCount(int steps)
	{
		if (SwingUtilities.isEventDispatchThread())
		{
			p_setStepCount( steps);
		}
		else
		{
			SwingUtilities.invokeLater(() -> p_setStepCount(steps));
		}
	}

	@Override
	public void progressNextStep(String msg)
	{
		if (SwingUtilities.isEventDispatchThread())
		{
			p_nextMessage( msg);
		}
		else
		{
			SwingUtilities.invokeLater(() -> p_nextMessage(msg));
		}
	}

	@Override
	public void progressReady()
	{
		if (SwingUtilities.isEventDispatchThread())
		{
			p_progressReady( );
		}
		else
		{
			SwingUtilities.invokeLater(this::p_progressReady);
		}
	}

	private void p_progressReady()
	{
		// thread-safe ?
		m_Progress.setValue( m_Progress.getMaximum());
		setVisible(false);
	}

	private void p_nextMessage( String msg)
	{
		m_Text.setText( msg);
		m_Progress.setValue( m_Progress.getValue()+1);
	}

	private void p_start( String title)
	{
		setTitle( title);
		setVisible(true);
	}

	private void p_setStepCount(int cnt)
	{
		m_Progress.setMaximum( cnt);
	}

	private final JProgressBar 	m_Progress;
	private final JTextField	m_Text;

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
}
