package de.ghoe.guenter.app.mcast.cmd.sys;

public class Cmd_GetFeatures extends CmdSys
{
	public Cmd_GetFeatures( String param)
	{
	}

	@Override
	public String getCmd()
	{
		return CMD_NAME;
	}

	public static final String CMD_NAME = "getFeatures";
	public static final String CMD_DESC = "Get list of available features";
}
