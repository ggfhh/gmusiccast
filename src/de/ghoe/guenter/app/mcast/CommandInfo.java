package de.ghoe.guenter.app.mcast;

public class CommandInfo
{
		public CommandInfo( String cmd, String description)
		{
			m_Cmd = cmd;
			m_Description = description;
		}
		
		public String getCommand()
		{
			return m_Cmd;
		}
		
		public String getDescription()
		{
			return m_Description;
		}
		
		private final String m_Cmd;
		private final String m_Description;
}
