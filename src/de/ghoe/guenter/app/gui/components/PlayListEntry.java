package de.ghoe.guenter.app.gui.components;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.util.List;

import de.ghoe.guenter.app.gui.IntfApplication;
import de.ghoe.guenter.app.mcast.data.ContentNode;
import de.ghoe.guenter.support.worker.IntfJobProgress;

public class PlayListEntry 
{
	public PlayListEntry( ContentNode node)
	{
		m_Title = node.getExtendedName();
		m_ContentNode = node;
	}

	public String toString()
	{
		return m_Title;
	}
	
	public ContentNode getContent()
	{
		return m_ContentNode;
	}
	
	public void save( BufferedWriter writer, int idx) throws IOException
	{
		ContentNode cn = getContent();
		List<Integer> refs = cn.getPositionPath();
		
		StringBuilder sb = new StringBuilder();
		sb.append( idx);
		sb.append( '=');
		sb.append( refs.size());
		sb.append( ':');
		for( Integer r: refs)
		{
			sb.append( ' ');
			sb.append( r);
		}
		sb.append( '\n');
		writer.write( sb.toString());
		List<String> n = cn.getStringPath();
		for (String s : n)
		{
			writer.write( String.format( "%s%s\n", STRING_REF_PREFIX, s));
		}
	}

	public static PlayListEntry s_read(IntfJobProgress progress, IntfApplication app, BufferedReader reader) throws IOException
	{
		String line = reader.readLine();
		int[] refInts = readRefIndex( line.replaceFirst( "^[^:]*:\\s*",""));
		String[] refStrings = readRefString( refInts.length, reader);
		progress.progressNextStep( getRefString( refStrings));
		ContentNode  cn = app.getContent( refStrings);
		return new PlayListEntry( cn);
	}

	private static String getRefString(String[] refStrings)
	{
		StringBuilder sb = new StringBuilder( "Load:");
		for (String s : refStrings)
		{
			sb.append( " -> ");
			sb.append( s);
		}
		return sb.toString();
	}

	private static String[] readRefString( int cnt, BufferedReader reader) throws IOException
	{
		String[] res = new String[cnt];
		for (int i = 0; i < cnt; ++i)
		{
			String line = reader.readLine();
			if (null == line) throw new IOException( "Playlist contain corrupt entry");
			if (!line.startsWith( STRING_REF_PREFIX)) throw new IOException( "Playlist missed a string reference");
			res[i] = line.substring( STRING_REF_PREFIX.length());
		}
		return res;
	}

	private static int[] readRefIndex( String in)
	{
		String[] strings = in.split( "\\s+");
		int[] ints = new int[strings.length];
		for (int i = 0; i < strings.length; ++i)
		{
			String s = strings[i];
			ints[i] = Integer.parseInt( s);
		}
		return ints;
	}

	private final String		m_Title;
	private final ContentNode m_ContentNode;

	private static final String		STRING_REF_PREFIX	= "\t->";
}
