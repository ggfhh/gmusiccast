package de.ghoe.guenter.app.gui;

import java.util.List;
import java.util.Vector;

import de.ghoe.guenter.app.mcast.cmd.Cmd;
import de.ghoe.guenter.app.mcast.data.ContentNode;
import de.ghoe.guenter.support.json.JsonHash;

public interface IntfApplication
{
	Vector<String> getCommands();
	void selectDevice( String deviceIp);
	ContentNode getContent();
	ContentNode getContent( String[] path);
	void executeCommand( String command, String param);
	JsonHash executeCommand( Cmd cmd) throws Exception;
	String getCommandDescription();

	void sendDeviceDiscover();
	
	String[] getPlayListNames();
	String[] getPlayListContent( int listIdx, String listName) throws Exception;
	ContentNode findContent( String name);

	boolean renamePlayList( int playListIndex, String newName);
	boolean clearPlayList( int playListIndex);
	boolean addNodeToPlaylist( int playListIndex, List<Integer> path);

	// read information
	List<ContentNode>	readFromPath( List<Integer> path, ContentNode parent);
}
