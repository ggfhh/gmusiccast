package de.ghoe.guenter.support.json;

import java.util.Vector;

import de.ghoe.guenter.support.json.support.StringParser;

public class JsonArray extends Json
{
	public JsonArray(String name, StringParser sp) throws Exception
	{
		super(name);
		
		m_Array = new Vector<>();
		if (!sp.startsWith( "[]"))
		{
			for (Json obj = s_createObject("[", sp); null != obj; obj = s_createObject( ",", sp))
			{
				m_Array.add( obj);
			}
		}
	}
	
	public int getEntryCount()
	{
		return m_Array.size();
	}

	public Json getJason( int idx)
	{
		return m_Array.get(idx);
	}
	
	public JsonBool getJasonBool( int idx)
	{
		return (JsonBool) m_Array.get(idx);
	}
	
	public JsonNumber getJasonNumber( int idx)
	{
		return (JsonNumber) m_Array.get(idx);
	}
	
	public JsonString getJasonString( int idx)
	{
		return (JsonString) m_Array.get(idx);
	}
	
	public JsonHash getJasonHash( int idx)
	{
		return (JsonHash) m_Array.get(idx);
	}
	
	public JsonArray getJasonArray( int idx)
	{
		return (JsonArray) m_Array.get(idx);
	}
	
	@Override
	public Object getObject()
	{
		return m_Array;
	}
	
	@Override
	public String getString()
	{
		return getText("");
	}
	
	@Override
	public String getText( String prefix)
	{
		StringBuilder sb = new StringBuilder( prefix);
		sb.append( "Array [");
		sb.append( m_Array.size());
		sb.append( "]\n");
		prefix += Json.PREFIX;
		for (int i = 0; i < m_Array.size(); ++i)
		{
			Json e = m_Array.get(i);
			sb.append( e.getText(String.format( "%s%d. ", prefix, i)));
		}
		return sb.toString();
	}
	
	private final Vector<Json>	m_Array;
}
