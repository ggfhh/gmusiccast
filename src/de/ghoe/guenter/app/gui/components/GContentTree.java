package de.ghoe.guenter.app.gui.components;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.List;
import java.util.ArrayList;

import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.border.BevelBorder;

import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

import de.ghoe.guenter.app.gui.components.support.GTreeModel;
import de.ghoe.guenter.app.mcast.data.ContentNode;
import de.ghoe.guenter.support.log.Log;

public class GContentTree 
{
	public GContentTree(JScrollPane parent, TreeNode root, MouseListener ml)
	{
		m_MouseListener = ml;
		m_Parent = parent;
		m_Content = root;
		setTree( );
	}
	
	public void setContent( TreeNode root)
	{
		if (m_Content != root)
		{
			m_Content = root;
			m_Parent.getViewport().remove( m_Tree);
			m_Parent.remove( m_Tree);
			setTree( );
		}
	}

	public List<ContentNode> getSelectedEntries()
	{
		List<ContentNode> l = new ArrayList<>();
		TreePath[] tp = m_Tree.getSelectionPaths();
		if (null != tp)
		{
			Log.s_logInfo("Get selected files count: " + tp.length);
			for (TreePath t : tp)
			{
				ContentNode cn = (ContentNode) t.getLastPathComponent();
				Log.s_logVerbose("Got node: " + cn.getPathAsString());
				l.add(cn);
			}
		}
		return l;
	}

	private void setTree( )
	{
		GTreeModel model = new GTreeModel(m_Content);
		m_Tree = new JTree(model);
		m_Tree.setEditable(false);
		m_Tree.getSelectionModel().setSelectionMode(TreeSelectionModel.DISCONTIGUOUS_TREE_SELECTION);
		m_Tree.setShowsRootHandles(true);
		m_Tree.setRootVisible(true);
		m_Tree.setBorder(new BevelBorder(BevelBorder.LOWERED));
		if (null != m_MouseListener)
		{
			m_Tree.addMouseListener(m_MouseListener);
		}
		m_Parent.getViewport().add(m_Tree);
	}

	private void clickEntry( MouseEvent e)
	{
		if (2 <= e.getClickCount())
		{
			Log.s_logWarning("Doubleclicked: ");
		}
	}

	private final JScrollPane 	m_Parent;
	private JTree				m_Tree;
	private TreeNode			m_Content;
	private final MouseListener	m_MouseListener;
}
