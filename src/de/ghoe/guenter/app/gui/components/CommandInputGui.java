package de.ghoe.guenter.app.gui.components;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JTextField;

import de.ghoe.guenter.app.gui.IntfApplication;

public class CommandInputGui extends JPanel implements ActionListener
{
	public CommandInputGui( IntfApplication app)
	{
		m_App = app;
		setLayout( new FlowLayout());
		m_CmdList = new JComboBox<>( app.getCommands());
		add( m_CmdList);
		m_Input = new JTextField( 40);
		add( m_Input);
		JButton send = new JButton("Send");
		send.addActionListener( this);
		add(send);
		JButton help = new JButton("?");
		help.addActionListener( (ActionEvent e) -> showHelp());
		add( help);
	}

	@Override
	public void actionPerformed(ActionEvent event)
	{
		Object obj = m_CmdList.getSelectedItem();
		if (null != (obj))
		{
			String cmd = obj.toString();
			String param = m_Input.getText();
			m_App.executeCommand(cmd, param);
		}
	}
	
	private void showHelp()
	{
		System.out.println( "Help Commands:");
		System.out.println( m_App.getCommandDescription());
	}

	private final IntfApplication 		m_App;
	private final JComboBox<String>		m_CmdList;
	private final JTextField			m_Input;

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
}
