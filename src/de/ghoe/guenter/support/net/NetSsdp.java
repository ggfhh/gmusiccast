package de.ghoe.guenter.support.net;

import java.io.IOException;
import java.net.*;
import java.util.List;

import de.ghoe.guenter.support.log.Log;

public class NetSsdp implements Runnable
{
	public NetSsdp(IntfNetSsdpReceive rx)
	{
		m_UpdateIf = rx;
	}

	public void broadcastDiscovery()
	{
		Thread t = new Thread( this);
		t.setDaemon(true);
		t.start();
	}

	@Override
	public void run()
	{
		Log.s_logInfo("SSDP-Thread is running");
		try
		{
			DatagramSocket sock = openSocket();
			try
			{
				sendDiscovery( sock);
				//noinspection InfiniteLoopStatement
				for (; ; )
				{
					DatagramPacket p = receiveReply( sock);

					Log.s_logVerbose("Receive content from SSDP socket");
					List<String> reply = NetHttp.s_getHttpContent( new String(p.getData(),0, p.getLength()));
					if (null != reply)
					{
						Log.s_logInfo("Accept SSDP reply from " + p.getSocketAddress().toString());
						m_UpdateIf.rxSsdpHostUpdate( p.getAddress());
					}
				}
			}
			catch (SocketTimeoutException te)
			{
				Log.s_logInfo("Timeout from SSDP-rx-thread (rx-timeout)");
			}
			catch (Exception e)
			{
				Log.s_log("Failed to handle SSDP-requests", e);
			}
			Log.s_logInfo("Shutdown SSDP-Rx-Thread");
			sock.close();
		}
		catch (SocketException e)
		{
			Log.s_log( "Failed to create SSDP-Socket", e);
		}
		Log.s_logInfo("SSDP-Handler finished");
	}

	private void sendDiscovery( DatagramSocket sock) throws IOException
	{
		Log.s_logVerbose("Send SSDP-Request from socket bound to: " + sock.getLocalAddress().toString());
		String data = "MAN: \"ssdp:discover\"\r\n" +
				"MX: 1\r\n" +
				"ST: urn:schemas-upnp-org:device:MediaRenderer:1\r\n\r\n";
		NetHttp.s_sendMSearchBroadcast(sock, "*", MCAST_ADDRESS, data, SSDP_PORT);
	}

	private DatagramSocket  openSocket() throws SocketException
	{
		DatagramSocket sock = new DatagramSocket();
		try
		{
			sock.setBroadcast(true);
			sock.setSoTimeout(SSDP_TIMEOUT);
			return sock;
		}
		catch(Throwable e)
		{
			sock.close();
			throw e;
		}
	}

	private DatagramPacket receiveReply( DatagramSocket sock) throws IOException
	{
		Log.s_logVerbose( "Wait for SSDP-Data");
		byte[] buffer = new byte[MAX_RX_SIZE];
		DatagramPacket p = new DatagramPacket(buffer, buffer.length);
		sock.receive(p);
		Log.s_logHex( Log.Type.Debug, "Got SSDP-Data from " + p.getSocketAddress().toString(), buffer, p.getLength());
		return p;
	}

	private final IntfNetSsdpReceive	m_UpdateIf;

	private static final int			MAX_RX_SIZE	= 0x10000;
	private static final int			SSDP_TIMEOUT = 1000;
	private static final int 			SSDP_PORT = 1900;
	private static final String			MCAST_ADDRESS = "139.255.255.250";
}
