package de.ghoe.guenter.app.gui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import de.ghoe.guenter.app.gui.components.DeviceListGui;
import de.ghoe.guenter.app.gui.components.GTabbedPane;
import de.ghoe.guenter.support.json.Json;

public class GMusicCastGui extends JFrame
{
	public GMusicCastGui(String title, String ver, IntfApplication intf) throws HeadlessException
	{
		super( title + " (Version " + ver + ")");
		setLayout(new BorderLayout());
		setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE);

		JPanel p = new JPanel();
		p.setLayout(new FlowLayout());
		m_DevList = new DeviceListGui( intf);
		p.add( m_DevList);
		JButton b = new JButton("Find");
		b.addActionListener( (ActionEvent e) -> discoverDevices( intf));
		p.add( b);
		add( p, BorderLayout.NORTH);

		add( createTabs( intf), BorderLayout.CENTER);
		
		m_State = new JLabel("State Startup");
		add( m_State, BorderLayout.SOUTH);
		pack();
	}
	
	public void setReply( String cmd, Json reply)
	{
		m_Tabs.getCurrentTab().setResult( cmd, reply);
	}
	
	public void setState( String state)
	{
		m_State.setText( state);
	}
	
	public void updateDeviceList(Vector<String> devs)
	{
		m_DevList.update(devs);
	}
	
	public void notifyDeviceChange()
	{
		GuiTab gt = m_Tabs.getCurrentTab();
		gt.selectCurrentTab();
	}
	
	private void discoverDevices( IntfApplication app)
	{
		app.sendDeviceDiscover();
	}
	
	private JTabbedPane createTabs(IntfApplication intf)
	{
		m_Tabs = new GTabbedPane();
		addTab( new GuiTab_Browse( this, intf));
		addTab( new GuiTab_Manual( this, intf));
		
		return m_Tabs;
	}
	
	private void addTab( GuiTab tab)
	{
		m_Tabs.add( tab.getTitle(), tab);
	}

	private GTabbedPane					m_Tabs;
	private final JLabel				m_State;
	private final DeviceListGui			m_DevList;
	/**
	 * 
	 */	
	private static final long serialVersionUID = 1L;
}
