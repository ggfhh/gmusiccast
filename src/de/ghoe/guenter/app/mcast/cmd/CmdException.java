package de.ghoe.guenter.app.mcast.cmd;

public class CmdException extends Exception
{
	public CmdException(String message)
	{
		super(message);
	}

	public CmdException(Throwable cause)
	{
		super(cause);
	}

	public CmdException(String message, Throwable cause)
	{
		super(message, cause);
	}

	public CmdException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
	{
		super(message, cause, enableSuppression, writableStackTrace);
	}
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
}
