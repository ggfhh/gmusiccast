package de.ghoe.guenter.app.mcast.data;

import de.ghoe.guenter.app.gui.IntfApplication;
import de.ghoe.guenter.app.mcast.cmd.CmdException;
import de.ghoe.guenter.app.mcast.cmd.netusb.Cmd_GetMcPlaylistName;
import de.ghoe.guenter.app.mcast.cmd.sys.Cmd_GetFeatures;
import de.ghoe.guenter.support.json.JsonHash;
import de.ghoe.guenter.support.json.JsonNumber;
import de.ghoe.guenter.support.log.Log;

public class PlayListInfo
{
	public PlayListInfo(IntfApplication app)
	{
		try
		{
			Cmd_GetFeatures cmd = new Cmd_GetFeatures("");
			JsonHash info = app.executeCommand( cmd);
			createPlayLists( app, info, cmd.checkResponse());
		}
		catch (Exception e)
		{
			Log.s_log( "Failed to read Playlist information", e);
			m_Names = new String[0];
			m_MaxSize = 0;
		}
	}

	public String[] getPlaylistNames()
	{
		return m_Names;
	}

	public int getPlaylistMaxSize()
	{
		return m_MaxSize;
	}

	private void createPlayLists( IntfApplication app, JsonHash info, boolean ok) throws Exception
	{
		if (ok)
		{
			JsonHash netUsb = info.getJasonHash( "netusb");
			if (null != netUsb)
			{
				JsonHash plInfo = netUsb.getJasonHash( "mc_playlist");
				if (null != plInfo)
				{
					JsonNumber cnt = plInfo.getJasonNumber("num");
					if (null != cnt)
					{
						JsonNumber size = plInfo.getJasonNumber("size");
						if (null != size)
						{
							m_MaxSize = size.getIntegerValue();
							loadPlayLists(app, cnt.getIntegerValue());
						}
					}
				}
			}
		}
	}

	private void loadPlayLists( IntfApplication app, int cnt) throws Exception
	{
		Cmd_GetMcPlaylistName cmd = new Cmd_GetMcPlaylistName("");
		app.executeCommand(cmd);
		if (cnt != cmd.getCount()) throw new CmdException("Playlist count doesn't match to playlist names");
		m_Names = cmd.getNames();
	}

	private String[]		m_Names;
	private int				m_MaxSize;
}
