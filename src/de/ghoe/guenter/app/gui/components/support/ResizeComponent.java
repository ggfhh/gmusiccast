package de.ghoe.guenter.app.gui.components.support;

import java.awt.Component;
import java.awt.Dimension;

public class ResizeComponent
{
	public static void s_setWidth( Component c, int width)
	{
		Dimension d = c.getPreferredSize();
		d.width = width;
		c.setPreferredSize( d);
	}
	public static void s_setHeight( Component c, int height)
	{
		Dimension d = c.getPreferredSize();
		d.height = height;
		c.setPreferredSize( d);		
	}
	public static void s_setSize( Component c, int width, int height)
	{
		Dimension d = new Dimension(  width, height);
		c.setPreferredSize( d);		
	}
}
