package de.ghoe.guenter.app.mcast.cmd.netusb;

public class Cmd_SetMcPlaylistName extends CmdNetUsb
{
    public Cmd_SetMcPlaylistName( String param)
    {
        String id = param.replaceFirst("\\s+.*","");
        String name = param.replaceFirst( "\\d+\\s*","");
        m_Ext = String.format( "{\"bank\":%s,\"name\":\"%s\"}", id, name);
    }

    public Cmd_SetMcPlaylistName( int listIdx, String name)
    {
        m_Ext = String.format( "{\"bank\":%d,\"name\":\"%s\"}", listIdx+1, name);
    }

    @Override
    public String getExtension()
    {
        return m_Ext;
    }

    @Override
    public String getCmd()
    {
        return CMD_NAME;
    }

    private static String       m_Ext;

    public static final String CMD_NAME = "setMcPlaylistName";
    public static final String CMD_DESC = "Rename play-list";
}
