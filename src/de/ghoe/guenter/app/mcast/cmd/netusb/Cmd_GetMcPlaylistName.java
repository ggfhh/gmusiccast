package de.ghoe.guenter.app.mcast.cmd.netusb;

import de.ghoe.guenter.app.mcast.cmd.CmdException;
import de.ghoe.guenter.support.json.JsonArray;
import de.ghoe.guenter.support.json.JsonHash;
import de.ghoe.guenter.support.json.JsonString;
import de.ghoe.guenter.support.net.NetHttpJson;

public class Cmd_GetMcPlaylistName extends CmdNetUsb
{
	public Cmd_GetMcPlaylistName( String param)
	{
	}

	@Override
	public String getCmd()
	{
		return CMD_NAME;
	}

	@Override
	public void execute( NetHttpJson net) throws Exception
	{
		super.execute( net);
		if (super.checkResponse())
		{
			JsonHash reply = super.getResult();
			JsonArray names = reply.getJasonArray("name_list");
			if (null != names)
			{
				int count = names.getEntryCount();
				m_Names = new String[count];
				for (int i = 0; i < count; ++i)
				{
					JsonString name = names.getJasonString(i);
					if (null != name)
					{
						m_Names[i] = name.getString();
					}
				}
			}
		}
	}

	public int getCount() throws CmdException
	{
		if (null == m_Names) throw new CmdException( "Command " + CMD_NAME + " not executed successfully");
		return m_Names.length;
	}

	public String[] getNames() throws CmdException
	{
		if (null == m_Names) throw new CmdException( "Command " + CMD_NAME + " not executed successfully");
		return m_Names;
	}

	private String[] 	m_Names;

	public static final String CMD_NAME = "getMcPlaylistName";
	public static final String CMD_DESC = "Get list of play-lists";


}
