package de.ghoe.guenter.support.json;

public class JsonString extends Json
{
	public JsonString( String name, String val)
	{
		super( name);
		
		m_Val = val;
	}

	@Override
	public Object getObject()
	{
		return m_Val;
	}

	@Override
	public String getString( )
	{
		return m_Val;
	}

	private final String m_Val;
}
