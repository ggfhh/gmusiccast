package de.ghoe.guenter.support.net;

public class HttpException extends Exception
{
	public HttpException(String message)
	{
		super(message);
	}
	public HttpException(String message, int errorCode)
	{
		super(message + " (" + errorCode + ")");
	}
	public HttpException(String message, Throwable cause)
	{
		super(message, cause);
	}
	
	public HttpException(String message, int errorCode, Throwable cause)
	{
		super(message + " (" + errorCode + ")", cause);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
