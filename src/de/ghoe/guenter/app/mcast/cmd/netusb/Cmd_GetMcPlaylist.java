package de.ghoe.guenter.app.mcast.cmd.netusb;

import de.ghoe.guenter.app.mcast.cmd.CmdException;
import de.ghoe.guenter.support.json.JsonArray;
import de.ghoe.guenter.support.json.JsonHash;
import de.ghoe.guenter.support.json.JsonNumber;
import de.ghoe.guenter.support.json.JsonString;
import de.ghoe.guenter.support.net.NetHttpJson;

public class Cmd_GetMcPlaylist extends CmdNetUsb
{
	public Cmd_GetMcPlaylist( String param) throws CmdException
	{
		String[] p = param.split("\\s+");

		switch( p.length)
		{
			case 0:
				m_Cmd = CMD_NAME + "?bank=0&index=0";
				break;
			case 1:
				m_Cmd = CMD_NAME + "?bank=" + p[0] + "&index=0";
				break;
			case 2:
				m_Cmd = CMD_NAME + "?bank=" + p[0] + "&index=" + p[1];
				break;
			default:
				m_Cmd = null;
				throw new CmdException("Invalid count of parameters for command " + CMD_NAME + ". Expect 0-3 parameters, got: " + param);
		}
	}

	public Cmd_GetMcPlaylist( int list)
	{
		m_Cmd = CMD_NAME + "?bank=" + (list+1) + "&index=0";
	}

	public Cmd_GetMcPlaylist( int list, int off)
	{
		m_Cmd = CMD_NAME + "?bank=" + (list+1) + "&index=" + off;
	}

	@Override
	public String getCmd()
	{
		return m_Cmd;
	}

	@Override
	public void execute( NetHttpJson net) throws Exception
	{
		super.execute( net);
		if (super.checkResponse())
		{
			JsonHash reply = super.getResult();

			JsonNumber count = reply.getJasonNumber("max_line");
			if (null == count) throw new CmdException( "Playlist has no max_line entry");
			int cnt = count.getIntegerValue();
			JsonArray tracks = reply.getJasonArray("track_info");
			if (null == tracks) throw new CmdException("Playlist has not track_info field");
			m_Names = new String[cnt];
			for (int i = 0; i < cnt; ++i)
			{
				JsonHash track = tracks.getJasonHash(i);
				if (null == track) throw new CmdException("Playlist missing track " + i);
				JsonString name = track.getJasonString( "text");
				if (null == name) throw new CmdException("Playlist entry " + i + " missing track name");
				m_Names[i] = name.getString();
			}
		}
	}

	public int getCount() throws CmdException
	{
		if (null == m_Names) throw new CmdException( "Command " + CMD_NAME + " not executed successfully");
		return m_Names.length;
	}

	public String[] getNames() throws CmdException
	{
		if (null == m_Names) throw new CmdException( "Command " + CMD_NAME + " not executed successfully");
		return m_Names;
	}

	private String[] 			m_Names;
	private static String		m_Cmd;

	public static final String CMD_NAME = "getMcPlaylist";
	public static final String CMD_DESC = "Get list of entries in playlists";
}