package de.ghoe.guenter.app.mcast.data;

public class ContentException extends Exception
{
	public ContentException(String message)
	{
		super(message);
	}
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
