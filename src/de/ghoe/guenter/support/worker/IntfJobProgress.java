package de.ghoe.guenter.support.worker;

public interface IntfJobProgress
{
	void progressStart( String title);
	void progressSetStepCount( int steps);
	void progressNextStep( String msg);
	void progressReady();
}
