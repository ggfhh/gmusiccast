package de.ghoe.guenter.app.mcast.cmd.netusb;

import de.ghoe.guenter.app.mcast.cmd.Cmd;

public abstract class CmdNetUsb extends Cmd
{
	public CmdNetUsb()
	{
		// TODO Auto-generated constructor stub
	}

	@Override
	public String getCmdGroup()
	{
		return "netusb";
	}

	@Override
	public abstract String getCmd();

	public static void s_addCommands( StringBuilder sb)
	{
		// add all net-usb commands
		s_addCommand( sb, Cmd_GetListInfo.class);
		s_addCommand( sb, Cmd_GetMcPlaylist.class);
		s_addCommand( sb, Cmd_GetMcPlaylistName.class);
		s_addCommand( sb, Cmd_GetSettings.class);
		s_addCommand( sb, Cmd_ManageList.class);
		s_addCommand( sb, Cmd_PresetInfo.class);
		s_addCommand( sb, Cmd_SetListControl.class);
		s_addCommand( sb, Cmd_SetSearchString.class);
	}
}
