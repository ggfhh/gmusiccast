package de.ghoe.guenter.support.config;

import de.ghoe.guenter.support.log.Log;

import java.awt.*;
import java.io.*;
import java.util.Properties;

public class UserConfig
{
	public static void s_save()
	{
		for (int idx = 0; ; ++idx)
		{
			File f = getConfigPath( idx);
			if (null == f) return;
			if (!f.exists() || f.canWrite())
			{
				try
				{
					FileWriter writer = new FileWriter(f);
					s_Config.store( writer, "GMusicCast Configuration");
					writer.close();
					s_ConfigPath = f;
					Log.s_logInfo("Successfully written configuration to '" + f.getAbsolutePath() + '\'');
					return;
				}
				catch (IOException e)
				{
					Log.s_log("Failed to write configuration", e);
				}
			}
		}
	}

	public static void s_set( String key, Dimension val)
	{
		s_set(key,String.format( "%d %d", val.width, val.height));
	}
	public static Dimension s_get( String key, Dimension def)
	{
		s_loadConfig();
		String res = s_Config.getProperty( key);
		if (null != res)
		{
			try
			{
				String[] data =  res.split( "\\s+");
				if (2 == data.length)
				{
					int w = Integer.parseInt( data[0]);
					int h = Integer.parseInt( data[1]);
					return new Dimension( w,h);
				}
			}
			catch (Throwable e)
			{
				Log.s_log("Failed to read Int-Config.", e);
			}
		}
		return def;
	}


	public static Dimension s_getFileChooserSize( Dimension def) { return s_get( FILECHOOSER_SIZE, def); }
	public static void s_setFileChooserSize( Dimension val)
	{
		s_set( FILECHOOSER_SIZE, val);
	}

	public static File s_getPlaylistFolder( File def)
	{
		return s_get( FILECHOOSER_FOLDER, def);
	}
	public static void s_setPlaylistFolder( File val)
	{
		s_set( FILECHOOSER_FOLDER, val);
	}

	public static Dimension s_getTreeviewSize( Dimension def) { return s_get( TREEVIEW_SIZE, def);}
	public static void s_setTreeviewSize( Dimension val)
	{
		s_set( TREEVIEW_SIZE, val);
	}

	public static Dimension s_getPlaylistSize( Dimension def) { return s_get( PLAYLIST_SIZE, def);}
	public static void s_setPlaylistSize( Dimension val)
	{
		s_set( PLAYLIST_SIZE, val);
	}

	//////////////////////////////////////////////////////////////////////////////////
	// Private data
	public static final String TREEVIEW_SIZE = "TreeViewSize";
	public static final String PLAYLIST_SIZE = "PlaylistSize";

	//////////////////////////////////////////////////////////////////////////////////
	// Private data
	private static final String FILECHOOSER_SIZE = "FileChooserSize";
	private static final String FILECHOOSER_FOLDER = "FileChooserFolder";

	//////////////////////////////////////////////////////////////////////////////////
	// Private methods
	private static String s_get( String key, String def)
	{
		s_loadConfig();
		String res  = s_Config.getProperty( key);
		return (null != res) ? res : def;
	}

	private static File s_get( String key, File path)
	{
		String s = s_get( key, path.getAbsolutePath());
		return new File( s);
	}

	private static void s_set( String key, String val)
	{
		s_loadConfig();
		String oldVal = s_Config.getProperty( key);
		if ((null == oldVal) || (!oldVal.equals( val)))
		{
			s_Config.setProperty(key, val);
		}
	}

	private static void s_set( String key, File val)
	{
		s_set( key, val.getAbsolutePath());
	}


	private static void s_loadConfig()
	{
		if (null == s_Config)
		{
			s_Config = new Properties();
			for (int idx = 0; ; ++idx)
			{
				File f = getConfigPath( idx);
				if (null == f) return;
				if (f.canRead())
				{
					try
					{
						FileReader reader = new FileReader(f);
						s_Config.load( reader);
						reader.close();
						s_ConfigPath = f;
						Log.s_logInfo("Successfully read configuration from '" + f.getAbsolutePath() + '\'');
						return;
					}
					catch (IOException e)
					{
						Log.s_log("Failed to read configuration", e);
					}
				}
			}
		}
	}

	private static File getConfigPath( int idx)
	{
		if (null == s_ConfigPath) ++idx;
		switch( idx)
		{
			case 0: return s_ConfigPath;
			case 1: return new File( "." + File.separator + CONFIG_FILE_NAME);
			case 2: return new File( System.getProperty( "user.dir") + File.separator + CONFIG_FILE_NAME);
			default: return null;
		}
	}

	private static final String	CONFIG_FILE_NAME	= ".GMusicCast.cfg";
	private static Properties	s_Config	 = null;
	private static File			s_ConfigPath = null;
}
