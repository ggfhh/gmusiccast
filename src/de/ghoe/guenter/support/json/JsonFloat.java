package de.ghoe.guenter.support.json;

public class JsonFloat extends Json
{
	public JsonFloat( String name, double val)
	{
		super(name);

		m_Val = val;
	}

	@Override
	public Object getObject()
	{
		return m_Val;
	}

	@Override
	public String getString( )
	{
		return m_Val.toString();
	}

	public Double getDoubleValue()
	{
		return m_Val;
	}
	
	private final Double m_Val;
}
