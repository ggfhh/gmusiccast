package de.ghoe.guenter.app.mcast.jobs;

import de.ghoe.guenter.app.gui.components.PlayList;
import de.ghoe.guenter.support.worker.IntfJobProgress;
import de.ghoe.guenter.support.worker.Job;

import java.io.File;
import java.io.IOException;

public class JobLoadPlaylist extends Job
{
	public JobLoadPlaylist(IntfJobProgress progress, String title, PlayList list, File f)
	{
		super(progress, title);
		m_List = list;
		m_File = f;
		start();
	}

	@Override
	public void doWork(IntfJobProgress progress) throws IOException
	{
		m_List.load( progress, m_File);
	}

	private final PlayList			m_List;
	private final File				m_File;
}
